<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>DinoEye - Project Management</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/my.css">
  <link rel="stylesheet" href="js/jquery-ui/jquery-ui.min.css">
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

  <!-- JAVASCRIPT -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/2.0.0/handlebars.js"></script>
  <style>
    .page {
      display:none;
    }
  </style>
</head>
<body>

  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="my_app">

  <div class="container">
    <div class="row">
      <div class="eight columns" >
        <div class="my_margin projects">

          <div class="all-clients page">
              <h4>All Clients</h4>
              <ul class="clients-list">
              <script id="clients-template" type="x-handlebars-template">​
                {{#each this}}
                <li data-index="{{id}}">
                  <b><a href="#"> {{name}} - {{id}} </a></b>
                  <ul class="client-description">
                      <li><span>Projects: </span>{{specs.projects}}</li>
                      <li><span>Milestones: </span>{{specs.milestones}}</li>
                    </ul>
                </li>
                <!--
                  <li data-index="{{id}}">
                    <a href="#" class="product-photo"><img src="{{image.small}}" height="130" alt="{{name}}"/></a>
                    <h2><a href="#"> {{name}} </a></h2>
                    <ul class="product-description">
                      <li><span>Manufacturer: </span>{{specs.manufacturer}}</li>
                      <li><span>Storage: </span>{{specs.storage}} GB</li>
                      <li><span>OS: </span>{{specs.os}}</li>
                      <li><span>Camera: </span>{{specs.camera}} Mpx</li>
                    </ul>
                    <button>Buy Now!</button>
                    <p class="product-price">{{price}}$</p>
                    <div class="highlight"></div>
                  </li>
                  -->
                {{/each}}
              </script> 
            </ul>
          </div>

          <div class="single-client page">
              <h4>Single Client</h4>
                <h2>...</h2>
                <li data-index="">
                  <b><a href="#"></a></b>
                  <ul class="client-description">
                      <li><span>Projects: </span>{{specs.projects}}</li>
                      <li><span>Milestones: </span>{{specs.milestones}}</li>
                    </ul>
                </li>
                <span class="close">&times;</span>
          </div> <!-- Page: Single client  -->

          <div class="error page">
            <h3>Sorry, something went wrong :(</h3>
          </div>


        </div>
      </div>
    </div>
  </div>

</div> <!-- my_app -->

  <div class="new_container">
      <!--This is our template. -->
      <!--Data will be inserted in its according place, replacing the brackets.-->
      <script id="address-template" type="text/x-handlebars-template">
         <!-- people is looked up on the global context, the one we pass to the compiled template -->
              {{#each people}}
                <!-- Here the context is each individual person. So we can access its properties directly: -->
                <p>{{firstName}} {{lastName}}</p>
              {{/each}}
      </script>

      <!--Your new content will be displayed in here-->
      <div class="content-placeholder"></div>
  </div>

  <div class="container">
    <div class="row">
      <div class="eight columns" >
        <div class="my_margin projects">
            <h4>Projects</h4>
            <hr />
            <div id="projects_content">
              <ul>
                <li>
                    <div class="proj_btn">+ Project 1</div>
                    <ul class="milestones">
                      <li> 
                        <a class="milestone" id="proj-1_milestone-1" title="Milestone 1" >Milestone 1</a>
                      </li>
                    </ul>
                </li>
                <li>
                    + Project 2
                    <ul class="milestones">
                      <li> 
                        <a class="milestone" id="proj-2_milestone-1" title="Milestone 1" >Milestone 1</a>
                      </li>
                    </ul>
                </li>
                <li>
                    + Project 3
                    <ul class="milestones">
                      <li> 
                        <a class="milestone" id="proj-3_milestone-1" title="Milestone 1" >Milestone 1</a>
                      </li>
                    </ul>
                </li>
              </ul>
            </div>

            <h4>Finished Projects</h4>
            <hr />
            <div id="finished_projects_content">
              <ul>
                <li>
                    <div class="proj_btn">+ Project 1</div>
                    <ul class="milestones">
                      <li> 
                        <a class="milestone" id="proj-1_milestone-1" title="Milestone 1" >Milestone 1</a>
                      </li>
                    </ul>
                </li>
                <li>
                    + Project 2
                    <ul class="milestones">
                      <li> 
                        <a class="milestone" id="proj-2_milestone-1" title="Milestone 1" >Milestone 1</a>
                      </li>
                    </ul>
                </li>
                <li>
                    + Project 3
                    <ul class="milestones">
                      <li> 
                        <a class="milestone" id="proj-3_milestone-1" title="Milestone 1" >Milestone 1</a>
                      </li>
                    </ul>
                </li>
              </ul>
            </div>
        </div>
      </div>
      <div class="four columns timestamps_bg" >
        <div class="my_margin">
          <!--
            <h4>TimeStamps</h4>
            <div class="milestone_name">Milestone name...</div>
            <hr />
            <div class="add_new_timestamp">
                <h6>Add new timestamp</h6>
                <form id="new_timestamp_form" action="backend/backend.php">
                  <input id="stamp_name" type="text" value="" placeholder="task..."/>
                  <input id="stamp_hours" type="text" value="" placeholder="hours..." />
                  <input id="datepicker" type="text" placeholder="date..." />
                  <input id="stamp_form_project_id" type="hidden" value="abc" />
                  <input id="stamp_form_milestone_id" type="hidden" value="abc" />
                  <input id="submit_stamp" type="submit" value="submit" />
                </form>
            </div>
            <div id="timestamp_content">
              <div class="timestamp">
                  Namn Efternamn<br />
                  2015-10-06 17:49:00
              </div>
            </div>
          -->
        </div>
      </div>
  </div> <!-- container -->

  <div class="overlay-bg">
  </div>
  <div class="overlay-content popup1">
      <p>Oh My! This is a popup!</p>
      <button class="close-btn">Close</button>
  </div>

  <script type="text/javascript" src="js/site.js"></script>

<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>
