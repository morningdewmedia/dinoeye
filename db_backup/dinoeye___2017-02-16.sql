# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.42)
# Database: dinoeye
# Generation Time: 2017-02-16 10:16:03 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table clients
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clients`;

CREATE TABLE `clients` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company` varchar(64) DEFAULT NULL,
  `contact_firstname` varchar(32) DEFAULT NULL,
  `contact_lastname` varchar(32) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `email_invoice` varchar(64) DEFAULT NULL,
  `currency` int(11) DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  `edate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;

INSERT INTO `clients` (`id`, `company`, `contact_firstname`, `contact_lastname`, `email`, `email_invoice`, `currency`, `cdate`, `edate`)
VALUES
	(1,'Come2play','Erez','Naveh','erez@come2play.com',NULL,3,NULL,NULL),
	(2,'Zordix','Matti','Larsson','matti@zordix.se',NULL,1,NULL,NULL),
	(3,'Karma Games','Alon','Faktor',NULL,NULL,3,NULL,NULL),
	(4,'Talang','Johan','Holmlund',NULL,NULL,1,NULL,NULL),
	(5,'Smith','Katarina','Hylander',NULL,NULL,1,NULL,NULL),
	(6,'HK','Kenth','Söderström',NULL,NULL,1,NULL,NULL),
	(7,'MeDent','Karin','Sunnegårdh','karin.sunnegardh@umu.se',NULL,1,NULL,NULL),
	(8,'SpinChem AB','Emil','Byström',NULL,NULL,1,NULL,NULL),
	(9,'Morningdew Media','Mattias','Holmgren',NULL,NULL,1,NULL,NULL),
	(10,'Teknikhuset','Anna-Maria',NULL,NULL,NULL,1,NULL,NULL),
	(11,'Turborilla','Tobitas','Andersson','tobias@turborilla.com','lin0963@pdf.grantthornton.se',1,'2015-12-10 09:00:00',NULL),
	(12,'Teknikmontage','Anders','Ludvigsson','anders.ludvigsson@teknikmontage.se','anders.ludvigsson@teknikmontage.se',1,'2015-12-11 09:14:00',NULL),
	(13,'Soja Film','Simon','Österhof','simon@sojafilm.se','simon@sojafilm.se',1,'2016-02-26 11:43:00',NULL),
	(14,'PGX','Per Gunnar','Eriksson','pg@pgxproduction.se','pg@pgxproduction.se',1,'2016-04-01 08:00:00',NULL),
	(15,'Evozon','Catalin','Zima-Zegreanu','catalin.zima@evozon.com','catalin.zima@evozon.com',1,'2016-05-16 08:09:00',NULL),
	(16,'Norra','Erika','Nilsson','erika.nilsson@norra.se','erika.nilsson@norra.se',1,'2016-06-01 00:00:00',NULL),
	(17,'E2Trading','Johan','Hellström','johan.hellstrom@t3.se','johan.hellstrom@t3.se',1,'2016-08-23 00:00:00',NULL),
	(18,'Oqurenz','Agnetha',NULL,NULL,NULL,1,'2016-09-29 08:00:00',NULL),
	(19,'Lipum','Susanne','Lindquist','susanne.lindquist@lipum.se','susanne.lindquist@lipum.se',1,'2017-02-02 08:00:00','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table currency
# ------------------------------------------------------------

DROP TABLE IF EXISTS `currency`;

CREATE TABLE `currency` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(16) DEFAULT NULL,
  `skr` int(11) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `currency` WRITE;
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;

INSERT INTO `currency` (`id`, `name`, `skr`, `value`, `cdate`)
VALUES
	(1,'SKR',1,1,'2015-09-17 09:43:00'),
	(2,'EURO',9,1,'2015-09-17 09:43:00'),
	(3,'USD',8,1,'2015-09-17 09:43:00');

/*!40000 ALTER TABLE `currency` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table currency_by_date
# ------------------------------------------------------------

DROP TABLE IF EXISTS `currency_by_date`;

CREATE TABLE `currency_by_date` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `currency_id` int(11) DEFAULT NULL,
  `skr` int(11) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `currency_by_date` WRITE;
/*!40000 ALTER TABLE `currency_by_date` DISABLE KEYS */;

INSERT INTO `currency_by_date` (`id`, `currency_id`, `skr`, `value`, `cdate`)
VALUES
	(1,1,1,1,NULL),
	(2,2,9,1,NULL),
	(3,3,8,1,NULL);

/*!40000 ALTER TABLE `currency_by_date` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table drive_journal
# ------------------------------------------------------------

DROP TABLE IF EXISTS `drive_journal`;

CREATE TABLE `drive_journal` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `milestone_id` int(11) DEFAULT NULL,
  `from` varchar(128) DEFAULT NULL,
  `to` varchar(128) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `distance_km` decimal(11,2) DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  `edate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `drive_journal` WRITE;
/*!40000 ALTER TABLE `drive_journal` DISABLE KEYS */;

INSERT INTO `drive_journal` (`id`, `project_id`, `milestone_id`, `from`, `to`, `start_time`, `end_time`, `distance_km`, `cdate`, `edate`)
VALUES
	(1,13,21,'Svampstigen 12, Holmsund','Teknikhuset, 907 36 Umeå','2015-10-19 08:30:00','2015-10-19 08:50:00',20.60,'2015-10-19 10:47:00',NULL),
	(2,13,21,'Teknikhuset, 907 36 Umeå','Svampstigen 12, Holmsund','2015-10-19 10:00:00','2015-10-19 10:35:00',20.60,'2015-10-19 10:48:00',NULL);

/*!40000 ALTER TABLE `drive_journal` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table invoice_emails
# ------------------------------------------------------------

DROP TABLE IF EXISTS `invoice_emails`;

CREATE TABLE `invoice_emails` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) DEFAULT NULL,
  `email_send_date` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `email_subject` text,
  `email_body` text,
  `cdate` datetime DEFAULT NULL,
  `edate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table invoice_row
# ------------------------------------------------------------

DROP TABLE IF EXISTS `invoice_row`;

CREATE TABLE `invoice_row` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `milestone_id` int(11) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  `edate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table invoices
# ------------------------------------------------------------

DROP TABLE IF EXISTS `invoices`;

CREATE TABLE `invoices` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `days_before_pay` int(11) DEFAULT NULL,
  `prepared_by_user_id` int(11) DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  `edate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table milestone_statuses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `milestone_statuses`;

CREATE TABLE `milestone_statuses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(64) DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `milestone_statuses` WRITE;
/*!40000 ALTER TABLE `milestone_statuses` DISABLE KEYS */;

INSERT INTO `milestone_statuses` (`id`, `status`, `cdate`)
VALUES
	(1,'Not started',NULL),
	(2,'In Progress',NULL),
	(3,'Ready to invoice',NULL),
	(4,'Invoice sent',NULL),
	(5,'Payment received',NULL),
	(6,'Invoice Reminder Sent',NULL),
	(7,'No pay',NULL);

/*!40000 ALTER TABLE `milestone_statuses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table milestones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `milestones`;

CREATE TABLE `milestones` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `milestone_name` varchar(128) DEFAULT NULL,
  `estimate_to_client` int(11) DEFAULT NULL,
  `price_per_hour` int(11) DEFAULT NULL,
  `price_per_project` int(11) DEFAULT NULL,
  `currency` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  `edate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `milestones` WRITE;
/*!40000 ALTER TABLE `milestones` DISABLE KEYS */;

INSERT INTO `milestones` (`id`, `project_id`, `milestone_name`, `estimate_to_client`, `price_per_hour`, `price_per_project`, `currency`, `status`, `cdate`, `edate`)
VALUES
	(1,1,'Godbiten Wp 2015',1,800,NULL,1,5,'2015-09-16 10:00:00',NULL),
	(2,2,'Nyåkers pepparkak',1,800,NULL,NULL,5,'2015-09-16 10:00:00',NULL),
	(3,3,'Toyota Flashbanner',8,800,NULL,NULL,4,'2015-09-16 14:00:00',NULL),
	(4,4,'Karma Games - Tournament Sounds',16,0,500,NULL,4,NULL,NULL),
	(5,5,'HK Rapport',0,650,0,1,5,'2015-09-18 08:03:00','2016-05-20 14:02:06'),
	(6,6,'MeDent, buggfix - 01.00 notifieringar.',0,0,0,NULL,5,NULL,NULL),
	(7,7,'Mining Safety - Menu music',0,800,0,NULL,5,NULL,NULL),
	(8,8,'Masterpiece - batch 3. 1sfx',3,0,150,3,4,'2015-10-01 14:00:00',NULL),
	(9,9,'Backgammon Live - Frankenstein',NULL,NULL,350,3,5,'2015-09-28 08:00:00',NULL),
	(10,9,'Backgammon Live - Hollywood',0,0,350,3,5,'2015-10-01 10:00:00',NULL),
	(11,7,'Mining Safety - SFX',0,0,0,1,5,'2015-10-02 08:00:00','2016-08-17 21:51:47'),
	(13,10,'Zordix webpage',8,0,0,1,5,'2015-10-01 08:00:00',NULL),
	(14,11,'SpinChem design',14,800,0,1,5,'2015-10-05 13:30:00',NULL),
	(15,11,'SpinChem Wordpress utv.',65,800,0,1,5,'2015-10-07 22:34:00',NULL),
	(16,12,'DinoEye',0,0,0,1,2,'2015-10-08 08:00:00',NULL),
	(17,6,'MeDent - Beröm anim - Toothguy',10,800,0,1,5,'2015-10-12 14:34:00',NULL),
	(18,6,'MeDent - idag / igår funktion.',7,800,0,1,5,'2015-10-12 12:45:00',NULL),
	(19,9,'Backgammon Live - Cuban Music',6,0,450,3,5,'2015-10-13 15:04:00',NULL),
	(20,9,'Backgammon Live - London',6,0,350,3,5,'2015-10-16 21:00:00',NULL),
	(21,13,'Norra - Vinjett intro 5sek',28,800,NULL,1,5,'2015-10-19 09:00:00',NULL),
	(22,13,'Norra - Avslut 2sek',10,800,NULL,1,5,'2015-10-19 09:00:00',NULL),
	(23,13,'Norra - Namnskylt',10,800,NULL,1,5,'2015-10-19 09:00:00',NULL),
	(24,9,'Backgammon Live - Paris',6,0,350,3,5,'2015-10-22 22:00:00',NULL),
	(25,9,'Backgammon Live - North Pole',6,0,350,3,5,'2015-10-25 23:00:00',NULL),
	(26,12,'MM - fakturering',0,0,0,NULL,5,'2015-10-26 08:30:00',NULL),
	(27,14,'Polarbröd pres.film',4,800,NULL,1,5,'2015-10-28 14:00:00','2015-11-04 08:30:00'),
	(28,6,'MeDent - BetaTestning',0,800,0,1,5,'2015-11-03 11:33:00','2016-07-25 12:06:58'),
	(29,12,'MM - Bokföring',0,0,0,1,5,'2015-11-12 00:00:00',NULL),
	(30,9,'Backgammon Live - Luxembourg',6,0,350,3,5,'2015-11-18 09:00:00','2015-11-19 08:35:00'),
	(31,9,'Backgammon Live - Moscow Russia',2,NULL,175,3,5,'2015-11-24 22:30:00',NULL),
	(32,15,'Möte ang. motorljud',1,800,0,1,5,'2015-12-10 09:00:00',NULL),
	(33,16,'Montage',0,360,0,1,5,'2015-12-11 09:15:00',NULL),
	(34,17,'AMR Utopia - Voice Over',18,800,0,1,5,'2015-12-10 08:00:00','2016-05-20 14:01:43'),
	(35,12,'Lek och lär med matte',0,0,0,1,5,'2015-12-14 08:00:00',NULL),
	(36,18,'Spinchem RBR Manual',6,800,0,1,5,'2015-12-16 08:00:00','2016-05-29 20:10:51'),
	(37,9,'BG Live - New Oreleans Jazz',3,0,175,3,5,'2015-12-22 10:00:00',NULL),
	(38,19,'Vezzo Wordpress plugin fix',3,800,0,1,4,'2016-01-04 09:50:00',NULL),
	(39,9,'BG Live - Rio Carnival',3,0,175,3,5,'2015-01-20 08:20:00','0000-00-00 00:00:00'),
	(40,18,'SpinC - RBR flöde illustration',2,800,0,1,5,'2016-01-27 08:20:00','2016-05-29 20:10:55'),
	(41,18,'SC Google Analytics Prod page',2,800,0,1,5,'2016-01-28 08:00:00','2016-05-29 20:10:58'),
	(42,18,'Presentationsdel',8,800,0,1,5,'2016-01-28 12:00:00','2016-05-29 20:11:05'),
	(43,18,'ModRewrite Fix old links',1,800,0,1,5,'2016-01-28 11:19:00','2016-05-29 20:11:02'),
	(44,19,'Vezzo hackfix, ta bort plugins.',0,800,0,1,4,'2016-02-09 08:35:00',NULL),
	(45,20,'Nya Ledare Ljudinspelning 2016-02-25',0,0,7000,1,5,'2016-02-25 08:00:00','2016-05-20 13:56:53'),
	(46,18,'SC - Webutveckling',0,800,0,1,5,'2016-02-29 10:00:00','2016-06-24 09:33:51'),
	(47,21,'EPS design',0,800,0,1,5,'2016-03-21 08:00:00','2016-05-20 13:57:08'),
	(48,1,'Godbiten WP fix',0,800,0,1,5,'2016-03-30 00:00:00','2016-05-20 14:01:58'),
	(49,21,'EPS extra design',0,800,0,1,5,'2016-04-05 08:38:00','2016-05-20 13:57:12'),
	(50,22,'PGX grafisk profil',0,800,0,1,4,'2016-04-04 08:00:00','2016-05-02 13:49:11'),
	(51,23,'CityGolf Design',8,800,0,1,5,'2016-04-11 08:00:00','2016-05-20 13:57:41'),
	(52,24,'BMX 2 musik.',0,800,0,1,5,'2016-04-13 08:00:00','2016-06-24 09:34:33'),
	(53,12,'Bokstavståg',0,800,0,1,2,'2016-04-18 08:00:00',NULL),
	(54,12,'shize bitte',2,800,2,1,1,'2016-04-27 00:00:00',NULL),
	(55,22,'PGX hemsida 01',0,800,0,1,4,'2016-04-27 08:00:00','2016-05-25 17:57:52'),
	(56,24,'BMX 2 sfx.',0,800,0,1,5,'2016-04-28 08:00:00','2016-06-24 09:34:38'),
	(57,20,'Nya Ledare',0,800,800,1,5,'2016-05-01 08:00:00','2016-06-24 09:31:12'),
	(58,25,'Separett meny',0,800,0,1,5,'2016-05-10 08:00:00','2016-07-25 12:06:20'),
	(59,26,' Norra design',40,800,0,1,5,'2016-05-13 08:00:00','2016-10-20 17:32:19'),
	(60,27,'Yaga Music',0,800,0,1,5,'2016-05-17 08:00:00','2016-07-25 12:06:05'),
	(61,22,'PGX hemsida 02 Juni',0,800,0,1,4,'2016-06-01 08:00:00','2016-06-30 21:28:59'),
	(62,28,'BG Menymusik',5,800,500,3,5,'2016-06-01 08:00:00','2016-07-25 12:05:47'),
	(63,1,'Godbiten SOS Barnbyar banner i footer',2,800,0,1,5,'2016-06-09 08:00:00','2016-08-17 21:50:10'),
	(64,29,'Skogens kretslopp animation',0,800,0,1,5,'2016-06-28 08:00:00','2016-10-20 17:46:29'),
	(65,30,'BG Searching Opponent',0,800,375,3,5,'2016-07-01 08:00:00','2016-08-17 21:52:12'),
	(66,30,'BG Roulette Sounds',0,800,200,3,5,'2016-07-01 08:00:00','2016-08-17 21:52:07'),
	(67,26,'Norra design augusti',0,800,0,1,5,'2016-08-08 08:00:00','2016-10-20 17:32:23'),
	(68,27,'Yaga Trailer Audio',0,800,0,1,2,'2016-08-12 08:00:00',NULL),
	(69,2,'Nyåkers justering av Om Oss',0,800,0,1,5,'2016-08-17 08:00:00','2016-10-20 17:52:01'),
	(70,31,'E2 logofix',1,800,0,1,5,'2016-08-23 08:00:00','2016-10-11 22:28:11'),
	(71,32,'BG daily bonus ljud',8,0,700,3,5,'2016-08-20 08:00:00','2016-10-20 17:46:14'),
	(72,26,'Norra Design September',0,800,0,1,5,'2016-08-30 08:00:00','2016-10-20 17:32:29'),
	(73,33,'Skogsskola Vinjett',80,800,0,1,5,'2016-08-29 08:00:00','2016-10-11 22:29:06'),
	(74,1,'Godbiten Cake hacks fix',0,800,0,1,5,'2016-08-30 08:00:00','2016-10-20 17:52:11'),
	(75,34,'BG - Pin Coin sfx',0,0,250,3,5,'2016-09-05 08:00:00','2016-10-11 22:28:28'),
	(76,33,'Skogsskola Vinjett Sept',0,800,0,1,5,'2016-09-07 08:00:00','2016-10-20 17:31:05'),
	(77,29,'Kretslopp anim - Sept',0,800,0,1,5,'2016-09-07 08:00:00','2016-10-20 17:46:33'),
	(78,35,'Vitec webb - Sept',0,800,0,1,5,'2016-09-13 08:00:00','2016-10-20 17:52:24'),
	(79,11,'SC utveckling Sept',0,800,0,1,5,'2016-09-14 08:00:00','2016-10-11 22:28:50'),
	(80,37,'SS - Skogsbr. klimatfrågan - Okt',0,800,0,1,4,'2016-09-14 08:00:00','2016-10-20 17:37:30'),
	(81,34,'BG - Mega Bonus Sounds',0,800,250,1,5,'2016-09-16 08:00:00','2016-10-11 22:28:23'),
	(82,11,'SC utveckling - sept 2',0,800,0,1,5,'2016-09-23 08:00:00','2016-10-11 22:28:45'),
	(83,29,'Kretslopp anim - Okt',0,800,0,1,5,'2016-09-25 08:00:00','2017-01-18 21:30:22'),
	(84,35,'Vitec webb - Okt',0,800,0,1,5,'2016-09-27 08:00:00','2017-01-24 09:45:53'),
	(85,36,'Oqurenz logotyp',8,800,0,1,4,'2016-09-28 08:00:00','2016-10-13 14:18:28'),
	(86,17,'Aqua Moto Ljudkomplettering',0,800,0,1,5,'2016-10-07 08:00:00','2017-01-24 09:46:37'),
	(87,11,'SC utveckling - okt',0,800,0,1,5,'2016-10-12 08:00:00','2017-01-24 09:45:21'),
	(88,17,'Aqua Moto VO projekt',0,800,0,1,5,'2016-10-14 08:00:00','2017-01-24 09:46:41'),
	(89,26,'Norra design okt',0,800,0,1,5,'2016-10-17 08:00:00','2017-01-24 09:44:24'),
	(90,24,'BMX sfx oct-nov',0,800,0,1,5,'2016-10-17 08:00:00','2017-01-25 20:31:48'),
	(91,29,'Kretsloppet - Nov',0,800,0,1,5,'2016-10-25 08:00:00','2017-01-18 21:30:27'),
	(92,26,'Norra design - Nov',0,800,0,1,5,'2016-10-25 08:00:00','2017-01-24 09:44:28'),
	(93,35,'VItec webb - nov',0,800,0,1,5,'2016-10-26 08:00:00','2016-12-22 11:41:18'),
	(94,2,'Nyåkers felsökning på produkt.',0,800,0,1,5,'2016-10-28 08:00:00','2017-01-24 09:45:11'),
	(95,11,'SC utv. nov',0,800,0,1,5,'2016-11-21 08:00:00','2017-01-24 09:45:25'),
	(96,37,'SS Klimatfrågan - Nov',0,800,0,1,4,'2016-10-08 08:00:00','2016-11-22 10:50:15'),
	(97,24,'BMX sfx dec',0,800,0,1,5,'2016-11-23 08:00:00','2017-01-25 20:31:55'),
	(98,29,'Kretslopp anim - dec',0,800,0,1,5,'2016-11-23 08:00:00','2017-01-18 21:30:32'),
	(99,37,'SS Klimatfråga - Dec',0,800,0,1,4,'2016-11-23 08:00:00','2016-12-22 11:41:34'),
	(100,26,'Norra design - Dec',0,800,0,1,5,'2016-11-25 08:00:00','2017-01-24 09:44:35'),
	(101,1,'Godbiten dec',0,800,0,1,2,'2016-12-01 08:00:00',NULL),
	(102,37,'SS Välfärd - Jan',0,800,0,1,4,'2017-01-01 08:00:00','2017-01-24 09:54:39'),
	(103,38,'SS Klimatnytta',0,800,0,1,4,'2017-01-01 08:00:00','2017-01-24 09:54:34'),
	(104,24,'BMX sfx jan',0,800,0,1,4,'2017-01-19 08:00:00','2017-01-24 09:54:29'),
	(105,37,'SS Välfärd - Feb',0,800,0,1,2,'2017-01-24 08:00:00',NULL),
	(106,38,'SS Klimatnytta - Feb',0,800,0,1,2,'2017-01-24 08:00:00',NULL),
	(107,24,'BMX SFX feb',0,800,0,1,2,'2017-01-24 08:00:00',NULL),
	(108,29,'Kretslopp bild - jan',0,800,0,1,2,'2017-01-31 08:00:00',NULL),
	(109,39,'Lipum design',0,800,0,1,2,'2017-02-01 08:00:00',NULL),
	(110,11,'SC utv. feb',0,800,0,1,2,'2017-02-01 08:00:00',NULL),
	(111,40,'Norra Play logo',0,800,0,1,2,'2017-02-14 08:00:00',NULL);

/*!40000 ALTER TABLE `milestones` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table notes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notes`;

CREATE TABLE `notes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `headline` varchar(256) DEFAULT NULL,
  `text` text,
  `cdate` datetime DEFAULT NULL,
  `edate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `notes` WRITE;
/*!40000 ALTER TABLE `notes` DISABLE KEYS */;

INSERT INTO `notes` (`id`, `headline`, `text`, `cdate`, `edate`)
VALUES
	(1,'Offert: TH - Norra skogsägarna','Vinjett intro till Norra skogsägarna. Dom bidrar med filmklipp. Jag klipper ihop en snygg animation och skriver musiksnutt.\n(2.5 dagar) = 20 h x 800 kr = 16.000kr.\nInväntar svar från Anna-Maria på TH.\n\nhttp://www.norra.se/pages/norraplay.aspx?ItemId=44','2015-10-13 09:37:00',NULL),
	(2,'Faktura: HK - Kenth','Kenth ska prata med HK VD eftersom dom tycker att dom inte fått ett färdigt system.\nSka ringa mig onsdag nästa vecka (ca 2015-10-21).','2015-10-12 11:45:00',NULL),
	(3,'Offert: TH - Norra skogsägarna','Ny offert baserat på 3 nya saker:\n1. Intro 5 sek\n2. Avslut 2 sek\n3. Namnskylt\n\nTid: 28h\nPris: 22.400kr','2015-10-21 10:00:00',NULL),
	(4,'PåminnelseFaktura - HK - Kenth','Skickar påminnelsefaktura med info om inkasso ifall fakturan inte betalas inom 10 dagar.','2015-10-26 10:49:00',NULL),
	(5,'Möte - Turborilla','Pratar 2-takts motorljud med Benjamin. Förespråkar enkelheten i accelleration och de-accellerations ljud istället för crossfading och up-pitchning av vanliga ljud.','2015-12-10 09:00:00',NULL),
	(6,'Betalar BilloGram','Billogramfaktura förfaller 2015-12-26, betalar på förfallodagen.','2015-12-26 22:15:00',NULL),
	(7,'Telmöte - HK - André ','Kommer överens med André om fortlöpande support på HK Rapport. 800kr / h. Uppstartad halvtimme.\nJag lovar att logga in på deras server och titta på loggfilen för HK Rapport och återkomma med problem jag ser.\nSka även ta en titt på abortUser() saveUser... uppkommer när sidan varit upp en stund och man försöker göra något.\nVad kan detta bero på?\n','2016-02-04 13:16:00',NULL);

/*!40000 ALTER TABLE `notes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table notes_connect
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notes_connect`;

CREATE TABLE `notes_connect` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `note_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `milestone_id` int(11) DEFAULT NULL,
  `timestamp_id` int(11) DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `notes_connect` WRITE;
/*!40000 ALTER TABLE `notes_connect` DISABLE KEYS */;

INSERT INTO `notes_connect` (`id`, `note_id`, `client_id`, `project_id`, `milestone_id`, `timestamp_id`, `cdate`)
VALUES
	(1,1,10,NULL,NULL,NULL,'2015-10-13 09:40:00'),
	(2,2,6,NULL,NULL,NULL,'2015-10-12 11:45:00'),
	(3,3,6,NULL,NULL,NULL,'2015-10-12 00:00:00'),
	(4,4,10,NULL,NULL,NULL,'2015-10-26 10:50:00'),
	(7,7,6,NULL,NULL,NULL,'2016-02-04 13:17:00');

/*!40000 ALTER TABLE `notes_connect` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table project_statuses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_statuses`;

CREATE TABLE `project_statuses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(64) DEFAULT '',
  `cdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `project_statuses` WRITE;
/*!40000 ALTER TABLE `project_statuses` DISABLE KEYS */;

INSERT INTO `project_statuses` (`id`, `status`, `cdate`)
VALUES
	(1,'Not started','2015-12-10 08:00:00'),
	(2,'In progress','2015-12-10 08:00:00'),
	(3,'Finished','2015-12-10 08:00:00'),
	(4,'Discontinued','2015-12-10 08:00:00');

/*!40000 ALTER TABLE `project_statuses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table projects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `description` text,
  `project_lead_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  `edate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;

INSERT INTO `projects` (`id`, `client_id`, `name`, `description`, `project_lead_id`, `status`, `cdate`, `edate`)
VALUES
	(1,4,'Godbiten WP',NULL,NULL,2,'2015-09-16 08:00:00','2015-09-16 08:00:00'),
	(2,4,'Nyåkers Pepparkakor WP',NULL,NULL,2,'2015-09-18 09:00:00',NULL),
	(3,5,'Toyota Flash Banner',NULL,NULL,3,'2015-09-16 13:00:00',NULL),
	(4,3,'Karma Games Slot',NULL,NULL,3,'2015-09-17 09:38:00',NULL),
	(5,6,'HK Rapport',NULL,NULL,3,'2015-08-01 00:00:00',NULL),
	(6,7,'MeDent',NULL,NULL,2,'2015-02-01 10:00:00',NULL),
	(7,2,'Mining Safety Education',NULL,NULL,3,'2015-09-28 10:00:00',NULL),
	(8,1,'Masterpiece Solitaire',NULL,NULL,3,'2015-10-01 09:00:00',NULL),
	(9,1,'Backgammon Live',NULL,NULL,3,'2015-09-25 08:00:00',NULL),
	(10,2,'Zordix Webpage',NULL,NULL,3,'2015-10-01 08:00:00',NULL),
	(11,8,'SpinChem Webpage',NULL,NULL,2,'2015-10-05 08:00:00',NULL),
	(12,9,'Morningdew Media Internal',NULL,NULL,2,'2015-10-11 08:00:00','0000-00-00 00:00:00'),
	(13,10,'Norra Skogsägarna vinjett',NULL,NULL,3,'2015-10-19 09:00:00',NULL),
	(14,10,'Polarbröd pres.film',NULL,NULL,3,'2015-10-28 14:00:00',NULL),
	(15,11,'Motocross 3 - ljud','2-takts motorljud till Moto Cross 3.',NULL,3,'2015-12-10 09:00:00',NULL),
	(16,12,'Teknikmontage - Montage','Installation och montage.',NULL,3,'2015-12-11 09:15:00',NULL),
	(17,2,'Aqua Moto Utopia',NULL,NULL,2,'2015-12-10 08:00:00',NULL),
	(18,8,'Spinchem RBR Manual',NULL,NULL,3,'2015-12-16 08:00:00',NULL),
	(19,4,'Vezzo',NULL,NULL,3,'2016-01-04 09:50:00',NULL),
	(20,13,'Soja Film - Nya Ledare',NULL,NULL,3,'2016-02-24 08:00:00',NULL),
	(21,12,'EPS web',NULL,NULL,3,'2016-03-21 08:00:00',NULL),
	(22,14,'PGX web + profil',NULL,NULL,3,'2016-04-05 08:00:00',NULL),
	(23,12,'CityGolf',NULL,NULL,3,'2016-04-11 08:00:00',NULL),
	(24,11,'BMX sound design',NULL,NULL,2,'2016-04-13 08:00:00',NULL),
	(25,12,'Separett meny',NULL,NULL,3,'2016-05-10 08:00:00',NULL),
	(26,12,'Norra design',NULL,NULL,2,'2016-05-13 08:00:00',NULL),
	(27,15,'Yaga Music',NULL,NULL,3,'2016-05-17 08:00:00',NULL),
	(28,1,'BG menymusik',NULL,NULL,3,'2016-06-01 08:00:00',NULL),
	(29,16,'Norra Skogens kretslopp','Skogens kretslopp - animation',NULL,2,'2016-06-28 08:00:00',NULL),
	(30,1,'BG July 2016',NULL,NULL,3,'2016-07-01 08:00:00',NULL),
	(31,17,'E2 logofix',NULL,NULL,3,'2016-08-23 00:00:00',NULL),
	(32,1,'BG August 2016',NULL,NULL,3,'2016-08-20 00:00:00',NULL),
	(33,16,'Skogsskolan Vinjett',NULL,NULL,3,'2016-08-29 08:00:00',NULL),
	(34,1,'Come2Play Sept 2016',NULL,NULL,3,'2016-09-01 08:00:00',NULL),
	(35,12,'Vitec Ny Webb',NULL,NULL,3,'2016-09-13 08:00:00',NULL),
	(36,18,'Oqurenz logotyp',NULL,NULL,3,'2016-09-28 08:00:00',NULL),
	(37,16,'SS Bidrag till välfärd 1-1',NULL,NULL,2,'2016-10-01 08:00:00',NULL),
	(38,16,'SS Klimatnytta 1-2',NULL,NULL,2,'2017-01-01 08:00:00',NULL),
	(39,19,'Lipum design',NULL,NULL,2,'2017-02-02 08:00:00',NULL),
	(40,12,'Norra Play Logo',NULL,NULL,2,'2017-02-14 08:00:00',NULL);

/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table stock_internal
# ------------------------------------------------------------

DROP TABLE IF EXISTS `stock_internal`;

CREATE TABLE `stock_internal` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` int(11) DEFAULT NULL,
  `ref_internal` varchar(64) DEFAULT NULL,
  `ref_external` varchar(64) DEFAULT NULL,
  `image` int(11) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `price_external` int(11) DEFAULT NULL,
  `stock_cat` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `cdate` int(11) DEFAULT NULL,
  `edate` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table time_stamps
# ------------------------------------------------------------

DROP TABLE IF EXISTS `time_stamps`;

CREATE TABLE `time_stamps` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `milestone_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `time_spent` decimal(11,2) DEFAULT NULL,
  `task_name` varchar(512) DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  `edate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`),
  KEY `milestone_id` (`milestone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `time_stamps` WRITE;
/*!40000 ALTER TABLE `time_stamps` DISABLE KEYS */;

INSERT INTO `time_stamps` (`id`, `project_id`, `milestone_id`, `user_id`, `time_spent`, `task_name`, `cdate`, `edate`)
VALUES
	(1,1,1,1,1.00,NULL,'2015-09-16 10:00:00',NULL),
	(2,2,2,1,1.00,NULL,'2015-09-16 13:30:00',NULL),
	(3,3,3,1,3.00,NULL,'2015-09-16 21:00:00',NULL),
	(4,3,3,1,3.00,NULL,'2015-09-17 08:00:00',NULL),
	(5,5,5,1,2.00,'Indexering av db, utvärdering av slö orderläggning (beror på epostserver, SMTP)','2015-09-18 08:03:00',NULL),
	(6,6,6,1,4.00,NULL,'2015-09-21 21:00:00',NULL),
	(7,6,6,1,4.00,NULL,'2015-09-22 08:00:00',NULL),
	(8,7,7,1,4.00,NULL,'2015-09-28 08:00:00',NULL),
	(9,7,7,1,4.00,NULL,'2015-09-29 08:00:00',NULL),
	(10,7,7,1,0.30,NULL,'2015-09-30 09:00:00',NULL),
	(11,10,13,1,7.00,'Fix ny meny, slideshow.','2015-10-02 08:30:00',NULL),
	(12,10,13,1,0.30,'Skiss ny meny','2015-10-03 09:30:00',NULL),
	(13,7,11,1,4.00,'','2015-10-05 08:00:00',NULL),
	(14,11,14,1,4.30,'Påbörjar design','2015-10-05 13:30:00',NULL),
	(15,9,10,1,4.00,NULL,'2015-10-06 08:00:00',NULL),
	(16,11,14,1,5.00,'Skiss 2 produktsidan','2015-10-07 08:00:00',NULL),
	(17,11,14,1,0.30,'Design','2015-10-07 13:30:00',NULL),
	(18,11,14,1,2.30,'prod.kat ikoner','2015-10-08 08:30:00',NULL),
	(35,11,14,1,0.30,'Prodkat. revision 2.','2015-10-08 23:25:58',NULL),
	(36,7,11,1,1.00,'Win, Lost, Correct, Wrong answer','2015-10-08 23:29:54',NULL),
	(37,8,8,1,1.00,'fixade grejjer','2015-10-09 13:01:40',NULL),
	(38,10,13,1,6.00,'Mobilslider, dynamic flip between desktop and mobile.','2015-10-09 15:24:48',NULL),
	(39,10,13,1,2.30,'Lägger upp på demo.mm.com. Småjustering på slider. Kollar olika enheter.','2015-10-11 22:18:08',NULL),
	(40,12,16,1,2.00,'Hours_spent vs total_hours on milestone.','2015-10-11 23:28:31',NULL),
	(41,10,13,1,0.30,'Flyttar menyn under slideshow.','2015-10-12 08:49:34',NULL),
	(42,11,14,1,1.30,'Quote me box sketch and flow.','2015-10-12 11:16:20',NULL),
	(43,6,18,1,0.30,'Skiss på idag/igår.','2015-10-12 14:35:58',NULL),
	(44,6,17,1,1.20,'Tootguy anim 1 & 2 with snow.','2015-10-12 14:36:37',NULL),
	(45,6,18,1,1.00,'Code for today / yesterday labels.','2015-10-12 21:14:40',NULL),
	(46,6,18,1,1.00,'Day marker re-position code for today / yesterday.','2015-10-13 09:13:51',NULL),
	(47,10,13,1,2.30,'Lägger temat live och testar få meny i toppen enbart på mobil.','2015-10-13 13:04:42',NULL),
	(48,6,18,1,1.50,'Calculating yesterdays date. Not ready...','2015-10-13 15:05:28',NULL),
	(49,9,19,1,4.00,'Compose Cuba Music in Logic.','2015-10-13 15:20:14',NULL),
	(50,6,18,1,2.20,'Today / yesterday flip OK. Next up is to save the data. New lighter btw sound added.','2015-10-14 10:50:10',NULL),
	(51,11,14,1,0.45,'Skapar design doc på google, delar med Tobias och Emil.','2015-10-14 11:36:21',NULL),
	(52,7,11,1,0.30,'Man screams','2015-10-14 14:01:17',NULL),
	(53,11,15,1,2.30,'Wp install, top menu css.','2015-10-14 15:27:00',NULL),
	(54,10,13,1,1.00,'Justerar höjd på orbit-slider med javascript annars blinkar sidan då höjden är noll vid load.','2015-10-15 09:29:07',NULL),
	(55,11,14,1,0.30,'Telemöte med Tobias ang. highlights m.m','2015-10-15 10:30:09',NULL),
	(56,11,15,1,1.00,'Menyn: cart.svg, logo size and font size.','2015-10-15 11:34:04',NULL),
	(57,11,15,1,0.30,'Minskar text vid mindre skärm.','2015-10-15 12:08:17',NULL),
	(58,11,15,1,3.00,'Splash css, style på discovery knapp.','2015-10-15 21:27:45',NULL),
	(59,6,18,1,1.00,'Nicklas Hjälper med sparning.','2015-10-16 11:35:40',NULL),
	(60,11,15,1,3.00,'Cart box css and script.','2015-10-16 16:32:05',NULL),
	(61,9,20,1,3.00,'Compose orchestral royal music.','2015-10-17 11:30:36',NULL),
	(62,13,21,1,1.00,'Möte med Anna-Maria (TH), letar bilder till vinjett.','2015-10-19 10:42:35',NULL),
	(63,11,15,1,6.00,'Cart box css och position, statisk menyposition, highlightboxar på startsidan..','2015-10-19 18:01:35',NULL),
	(64,6,17,1,3.30,'Toothguy,moln,shine på Min Resa. Timer för blinkning.','2015-10-20 11:46:00',NULL),
	(65,6,17,1,0.45,'Snöeffekt bakom Toothguy.','2015-10-20 12:17:37',NULL),
	(66,11,15,1,0.20,'Email ang. bildstorlekar m.m.','2015-10-21 08:37:20',NULL),
	(67,6,17,1,3.50,'5 varianter på toothguy animation, ljudlägger.','2015-10-21 13:15:58',NULL),
	(68,6,17,1,2.00,'Visa bara toothguy om man registrerat bättre värde än tidigare. Spara userDefault xml. Fixa så OK knapp visas vid notification i MainMenu.','2015-10-22 10:39:14',NULL),
	(69,6,18,1,0.20,'Scolla upp om man flippar idag/igår.','2015-10-22 10:39:42',NULL),
	(70,11,15,1,0.30,'Påbörjar footer meny CSS.','2015-10-21 10:41:00',NULL),
	(75,12,16,1,1.00,'Möjlighet att välja datum vid skapande av timestamp.','2015-10-22 13:43:08',NULL),
	(76,7,11,1,1.00,'woman_screams by Malin Holmgren.','2015-10-22 15:06:50',NULL),
	(77,9,24,1,7.00,'Skriver tango till Paris, accordion m.m.','2015-10-22 08:37:10',NULL),
	(78,9,9,1,4.00,'Spooky, mad scientist orchestral for Frankenstein.','2015-09-16 10:37:50',NULL),
	(79,11,15,1,1.00,'Footer CSS','2015-10-23 09:31:09',NULL),
	(80,11,15,1,2.50,'Footer Backend','2015-10-23 11:29:10',NULL),
	(81,11,15,1,1.20,'Footer CSS mobile, Products setup in admin.','2015-10-23 15:02:57',NULL),
	(82,11,15,1,0.20,'Product cat bg image and center div fix.','2015-10-23 22:35:33',NULL),
	(83,9,25,1,5.00,'Re-arrange Minion Stroll for North Pole level.','2015-10-25 08:51:03',NULL),
	(84,12,26,1,2.00,'Skickar påminnelsefakturor: HK Kenth, Johan Holmlund. Skickar faktura till Come2Play.','2015-10-26 10:58:02',NULL),
	(85,11,15,1,1.30,'Product page, product items list påbörjas.','2015-10-26 13:27:22',NULL),
	(86,6,17,1,2.00,'Fixar så ToothGuy visas även första gången man reggar daglig skörd för varje dag.','2015-10-26 13:27:53',NULL),
	(87,11,15,1,3.00,'Product List Items css.','2015-10-26 21:44:37',NULL),
	(88,11,15,1,1.30,'Product accessories koppling i admin. Mailar Spinchem om status på projektet.','2015-10-27 10:24:44',NULL),
	(89,11,15,1,1.20,'List products from backend. Contact & about page info added.','2015-10-27 12:36:17',NULL),
	(90,11,15,1,4.00,'Quote button blue color, Prod Nr position, + and -, Quote Cart backend 75% done.','2015-10-27 00:26:53',NULL),
	(91,11,15,1,1.40,'Add new specs, show only specs with data, close product via prod-thumb click.','2015-10-28 12:08:20',NULL),
	(92,6,18,1,2.00,'MeDent Android compile test.','2015-10-28 12:09:04',NULL),
	(93,6,17,1,2.00,'Mergar kod från nickes och mitt project. Bygger till Android.','2015-10-28 12:01:30',NULL),
	(94,14,27,1,3.00,'Klipper ihop film, skapar animation i Motion.','2015-10-28 12:10:55',NULL),
	(95,14,27,1,1.00,'Klipper om enligt instruktion från Polarbröd. Lägger in ny text.','2015-10-29 12:11:15',NULL),
	(96,11,15,1,1.00,'Minskar utrymme på specs i admin.','2015-10-28 12:12:53',NULL),
	(97,6,18,1,2.00,'Justerar Toothguy pos, today / yesterday pos. Bygger android .apk och skickar till Karin.','2015-10-29 21:05:19',NULL),
	(98,11,15,1,1.50,'Pdf ikon svg och justerar knappgrafik så det ska se skarpare ut.','2015-10-29 21:06:01',NULL),
	(99,14,27,1,0.50,'Justerar så text tonas ett stycke i taget.','2015-10-29 21:06:57',NULL),
	(100,11,15,1,2.30,'Inkluderar prettyPhoto och får vimeo videos att fungera på produkter.','2015-10-29 00:01:54',NULL),
	(101,11,15,1,5.00,'Download popup, Ta bort produkt från quotekorg, vimeo spelare.','2015-10-30 09:52:58',NULL),
	(102,11,15,1,2.30,'Mobile Menu','2015-10-31 17:09:52',NULL),
	(103,11,15,1,2.50,'Mod Rewrite for product categories.','2015-11-02 12:02:58',NULL),
	(104,11,15,1,1.00,'Add Depth Spec, Fixar Quote icon in Add To Quote btn.','2015-11-02 23:12:07',NULL),
	(105,11,15,1,1.00,'Fixar så inte Cart Btn gennar 10000 clicks. Blev tvungen att göra en javascript click handler på en klass till a-taggen.','2015-11-03 00:11:13',NULL),
	(106,6,28,1,0.20,'Skapar sida med UDID instruktioner och skickar Karin.','2015-11-03 11:34:27',NULL),
	(107,11,15,1,3.50,'Accessories Links in product view. Created Highlight categories. Contact form almost done.','2015-11-03 16:44:05',NULL),
	(108,11,15,1,3.50,'Contact Form, email is now sent. Popup box for form errors and success. Pretty photo included in plugin instead of theme fixed error with Media Gallery in admin. Sent status update to Emil & Tobias.','2015-11-03 22:41:08',NULL),
	(109,11,15,1,3.00,'Application page setup, email Tobias regarding highlight-categories.','2015-11-04 15:06:25',NULL),
	(110,11,15,1,2.00,'Blockquote style in Applications Page, read Highlights from DB, Highlights CSS, Details expand, svg icons for the hl-types.','2015-11-04 15:07:37',NULL),
	(111,11,15,1,3.00,'Highlights / Products on Home Page, Applications layout, Email icon etc.','2015-11-04 00:06:11',NULL),
	(112,13,21,1,2.00,'Skapar lite mockup på storyboard och bilder / klipp / linjer, skickar till Anna Maria på TH.','2015-11-05 10:33:24',NULL),
	(113,11,15,1,1.00,'Sales Title, Sales text, sales image på prod /highlights, lightbox, ','2015-11-05 16:23:09',NULL),
	(114,11,15,1,3.30,'Single product page, single highlight page, make cart work on single page. Added product number to Quote Cart and Email.','2015-11-05 16:24:06',NULL),
	(115,11,15,1,2.20,'Videofix (svart bar), Components på Starter Kits i frontend. Permalinks på Accessories & Components.','2015-11-05 23:48:28',NULL),
	(116,11,15,1,4.00,'Spinchem backup of old site, new site goes live. Adjust buttons so they are not buggy. Adjust link colors on site. Make sure live site looks nice. Creates email for webserver@spinchem.com','2015-11-06 13:51:30',NULL),
	(117,11,15,1,3.15,'Applications (highlights layout), show more images, link list layout fix. Minskar plats på LinkList, Ny spec: Power Supply.','2015-11-09 20:26:32',NULL),
	(118,11,15,1,4.00,'Visa products på Highlight. Remove product from Quote. box via trash icon. Rename Highlight Custom Post to Application Custom Post.','2015-11-10 17:06:26',NULL),
	(119,11,15,1,3.00,'Phone meeting Tobias, hide image text at the bottom of lightbox. Hide multi images, layout fix on Applications. Adjustments according to phone meeting.','2015-11-10 17:08:25',NULL),
	(120,11,15,1,0.15,'Application Collection List begin.','2015-11-10 17:26:14',NULL),
	(121,11,15,1,1.00,'Application List smaller size.','2015-11-10 20:46:10',NULL),
	(122,11,15,1,0.30,'Technology Page setup.','2015-11-10 21:20:11',NULL),
	(123,11,15,1,0.20,'Products, connect Applications and show them.','2015-11-10 21:49:30',NULL),
	(124,11,15,1,0.45,'Headings in Technology / Application editable in admin. Adjust css for page layout on mobile.','2015-11-11 08:48:22',NULL),
	(125,11,15,1,0.15,'Fixar Accessories på single product page,','2015-11-11 22:02:48',NULL),
	(126,12,29,1,2.00,'Bokföring på MM enskild.','2015-11-11 12:17:48',NULL),
	(127,12,29,1,4.00,'Bokföring på MM AB.','2015-11-12 12:18:00',NULL),
	(128,11,15,1,3.00,'FAQ page, custom posts faq, list all pdfs.','2015-11-12 22:53:38',NULL),
	(129,11,15,1,0.30,'Vimeo embed Technology Page.','2015-11-13 11:13:04',NULL),
	(130,11,15,1,2.15,'User Interaction (plugin), show user email and which files the user downloaded.','2015-11-13 11:13:59',NULL),
	(131,13,21,1,2.00,'Skapar cue 01, piano och lite stråk. Stort.','2015-11-13 10:54:14',NULL),
	(132,13,21,1,2.30,'Cue 02, piano, strings, wind, epic! Sent to AnnaMaria.','2015-11-16 10:55:01',NULL),
	(133,11,15,1,0.30,'Fixar korrupt Rotating Bed Reactor svg. Justerar css så doc_icons för knappar ska se bättre ut på IE / Edge browser.','2015-11-16 11:24:20',NULL),
	(134,13,21,1,2.00,'Testar animation med linjer i Motion.','2015-11-16 20:45:52',NULL),
	(135,13,21,1,3.00,'På Teknikhuset, möte med Norra Skogsägarna. Visar musiktema och förslag på vinjett. OK från kund att gå vidare.','2015-11-17 23:09:03',NULL),
	(136,11,15,1,0.20,'Fix 166x166px thumbnails. Fix download documents.','2015-11-17 23:09:49',NULL),
	(137,11,15,1,1.00,'Påbörjar search','2015-11-18 08:02:44',NULL),
	(138,11,15,1,2.00,'Gör klart sökning, custom loop-search page och custom faq single page.','2015-11-19 12:25:06',NULL),
	(139,13,21,1,5.00,'Redigerar nya bilder och klipper om i Motion. Laddar upp till TH.','2015-11-23 10:52:01',NULL),
	(140,13,22,1,4.00,'Skapar avslutet och visar på dagens möte. Norra säger OK och vill ha snabb leverans idag.','2015-11-25 13:17:41',NULL),
	(141,13,21,1,4.00,'Lägger in en extra skogsbild och gör en testanimation av hjortron till logo.','2015-11-24 13:18:31',NULL),
	(142,9,31,1,3.50,'Writes a cue similar to Nutcracker (Tchaikovsky)','2015-11-24 13:21:36',NULL),
	(143,8,30,1,3.50,'Creates the Luxembourg cue.','2015-11-19 13:22:30',NULL),
	(144,13,23,1,5.00,'Möte + namnskylt på kvällen.','2015-11-25 01:53:16',NULL),
	(145,13,23,1,3.00,'Skapar png sekvens av namnskylt eftersom Norra ej kan ta emot ProRes442 formatet med alpha. Åker in till Karolina på Norra och visar hur hon lägger in sekvensen.','2015-11-26 15:10:01',NULL),
	(146,11,15,1,0.20,'Fix på titel för bilder på single application page, tar bort morningdewmedia mail från contact page.','2015-11-26 22:43:29',NULL),
	(148,13,21,1,3.00,'Mail från Karolina. För mycket 70tal, ta bort retroeffekter på bilder.','2015-11-30 23:15:23',NULL),
	(149,13,21,1,3.00,'Reviderar introt (tar bort light leaks), snyggare rök i slutet. Nya bilder i introt. Skickar nya filer till Karolina. Inväntar svar.','2015-12-01 08:18:11',NULL),
	(150,13,21,1,1.00,'Redigerar bild och lägger in skogsmaskin i intro. Skickar ny version till Karolina.','2015-12-07 16:12:04',NULL),
	(151,15,32,1,1.00,'Träffar Benjamin och för lite diskussion kring motorljud för Moto Cross 3. 2-takts motorljud, jag rekommenderar att köra på 1st accelerationsljud (riktigt) och 1 de-accellerationsljud.','2015-12-10 11:30:38',NULL),
	(152,16,33,1,7.30,'Montering på Obbola. Stoppdag.','2015-12-08 09:16:15',NULL),
	(160,12,29,1,2.00,'Betalar in prelkatt för MM AB, restskatt för MM enskild firma, tar ut tidigare lön inför jul, Betalar in skatt för januarilönen som flyttades från MM till MM AB.','2015-12-11 15:13:33',NULL),
	(161,12,26,1,0.30,'Får betalt från Smith International, skriver en avanmälan på betalningsbegäran till kronofogden.','2015-12-11 15:14:36',NULL),
	(162,17,34,1,3.00,'Big Steve Voice recording at Hemvägen 1.','2015-12-09 13:25:33',NULL),
	(163,17,34,1,2.00,'Testar VO i videoclip av Aqua Moto. Mixar ut 2 clip-varianter.','2015-12-10 13:26:17',NULL),
	(164,12,29,1,1.00,'Skriver invändning mot förseningsavgift till Skatteverket.','2015-12-14 09:18:33',NULL),
	(165,12,35,1,1.30,'* Hur mycket \"är det\" istället för \"blir det\".','2015-12-13 09:20:25',NULL),
	(166,17,34,1,4.00,'Klipper Big Steve VO.','2015-12-14 21:29:29',NULL),
	(167,17,34,1,5.50,'Klipper Big Steve VO.','2015-12-15 21:30:07',NULL),
	(168,18,36,1,2.00,'Mockup av manual i in-design. Skickar till Emil och väntar på svar.','2015-12-16 14:42:47',NULL),
	(169,17,34,1,0.30,'Klipper dom sista filerna och laddar upp till Zordix FTP.','2015-12-16 14:43:20',NULL),
	(170,18,36,1,2.00,'Uppdaterar manualen till A5, byter ut typsnitt lägger in alla sidor med nya bilder etc. Mailar Email.','2015-12-18 18:35:25',NULL),
	(171,9,37,1,3.00,'Compose jazz music','2015-12-21 22:08:45',NULL),
	(172,19,38,1,3.00,'Fixar ny slider, permalinks, helhet bildlänk error, Facebook plugin m.m.','2016-01-04 21:53:19',NULL),
	(174,18,36,1,2.00,'Vektoriserar bilder för RBR i manualen.','2016-01-13 12:48:06',NULL),
	(175,18,36,1,3.00,'Vektoriserar mer bilder och gör instruktion med hand för hur man viker filtret på punkt 6.','2016-01-14 12:48:38',NULL),
	(176,9,39,1,4.00,'Compose carnival rio music.','2016-01-20 09:11:32',NULL),
	(177,19,38,1,2.00,'Jobbat lite','2016-01-25 10:54:47',NULL),
	(178,18,36,1,1.00,'Möte med Emil och Tobias ang. ny beställning. Presentationssida, Google analyskod på produktsidans klick, ny vektorbild m.m.','2016-01-25 13:59:20',NULL),
	(179,18,40,1,2.00,'Vektoriserar rev 1 av Spinnhem Principle, flöde och mejlar Emil.','2016-01-27 11:51:32',NULL),
	(180,18,41,1,2.30,'GA Track Events på Produktsidan & Downloads popup.','2016-01-28 11:18:37',NULL),
	(181,18,42,1,2.50,'Skapar presentationssidan, css för film och textblock, bild.','2016-01-28 18:36:52',NULL),
	(182,5,5,1,1.00,'Möte med Mattias Forsman ang. Kenth, support och fortsatt verksamhet.','2016-01-29 11:16:24',NULL),
	(183,18,42,1,1.00,'Adds small_image for a image to the left of the paragraph.','2016-02-01 11:07:28',NULL),
	(184,18,40,1,0.30,'Sharper line at the cut-through of the image.','2016-02-01 11:08:05',NULL),
	(185,18,42,1,2.20,'Multiple Presentations.','2016-02-02 21:35:35',NULL),
	(186,18,41,1,0.15,'Google Analytics Events fix, needed small label (event).','2016-02-02 21:36:25',NULL),
	(187,18,43,1,1.00,'ModRerwrites according to xls doc from Tobias at Spinchem. Confirm that they work live.','2016-02-03 15:09:49',NULL),
	(188,12,35,1,3.00,'Localize app name, localise menu texts and parent texts, localize logo image.','2016-02-04 15:14:43',NULL),
	(189,18,40,1,0.30,'Klipper ut olika varianter av RBR skissen enligt önskemål från Emil.','2016-02-04 15:51:48',NULL),
	(190,19,44,1,1.00,'Tar bort hackade filer från Vezzo.se. Tar bort revslider och några andra plugins. Mailar Johan att kontakta loopia som får ta bort filer som ej gick att ta bort.','2016-02-08 08:36:34',NULL),
	(191,18,42,1,0.30,'Tar bort comments på attachments..','2016-02-09 10:46:47',NULL),
	(192,18,36,1,4.00,'1st illustr. RBR fyller på med vätska, 1st illuster. cartridge. Påbörjar illustration för skruvverktyg RBR.','2016-02-10 20:34:05',NULL),
	(193,18,36,1,1.00,'Gör klart illustr. för skruvverktyg 13.1','2016-02-10 21:32:47',NULL),
	(194,18,42,1,1.30,'Presentation - Toggle between regular scroll view and Slideshow view with navigation.','2016-02-12 09:48:45',NULL),
	(195,6,28,1,2.00,'Add UDID from Sara (MeDent beta tester). Add provision profile and build project.','2016-02-15 11:17:30',NULL),
	(196,18,36,1,3.00,'Nytt index. Gör om framsidan, försöker fixa fonten Intestate. ','2016-02-16 11:53:48',NULL),
	(197,18,42,1,1.00,'Swipeslider (vid slideshow läge). Fixar responsive min-width på videos så iPad ser mer korrekt ut i portrait m.m.','2016-02-17 09:55:14',NULL),
	(198,18,36,1,1.00,'Möte ang. nya illustrationer för patentansökan. Går igenom lite ändringar på hemsidan m.m.','2016-02-18 21:05:21',NULL),
	(199,18,36,1,2.00,'Revision på manualen, framsidan, vail, cartridge, verktyg, färger m.m.','2016-02-18 21:06:43',NULL),
	(200,18,36,1,2.30,'Byter typsnitt till Open Sans, fixar ljusare illustrationer.','2016-02-19 10:36:54',NULL),
	(201,18,42,1,1.00,'Fixar blå nav-arrows på slideshow. Fixar text above image if featured image exists.','2016-02-19 12:49:22',NULL),
	(202,18,36,1,2.00,'Fix på manual enligt mail från Tobias (19 feb Motor stand -> Assemble Reactor stand etc.','2016-02-22 23:14:59',NULL),
	(203,18,40,1,3.00,'Påbörjar RBR Patent illustr.','2016-02-22 23:15:53',NULL),
	(204,18,40,1,2.15,'RBR Pat ill. Ny vortex, skapar nr 5-10.','2016-02-23 10:52:43',NULL),
	(205,18,40,1,2.00,'RBR Pat ill. revision (ta bort lite bilder), klipper ut leveransfiler .ai, .png etc.','2016-02-23 14:57:15',NULL),
	(206,18,42,1,2.00,'Ändrar så enbart länkar är blå (ej headers), tar bort read me. css','2016-02-23 14:58:44',NULL),
	(207,18,42,1,1.30,'Ikon för Presentation_icon.sv. Fixar så splash kan länka till URL (discover pres.).','2016-02-24 09:38:13',NULL),
	(208,18,42,1,1.00,'Kodar in social icons i footer och admin->settings->general','2016-02-25 13:34:57',NULL),
	(209,18,42,1,1.00,'Gör om ikoner så de ser ut som knappar enligt iOS. Fixar så bilder får korrekt storlek även med senaste wordpress (blev för små bilder på iPad).','2016-02-26 10:43:42',NULL),
	(210,20,45,1,8.00,'Jonas och Hans på Umeå Kommun. Kamera Lampray Martin Gärdemalm, director Amanda.','2016-02-25 11:46:30',NULL),
	(211,18,46,1,1.00,'Knapp för Download Brochure i Presentationer (Slides).','2016-02-29 11:27:09',NULL),
	(212,18,46,1,2.00,'Fixar så text hamnar under bild på Slides. Fixar storlek på videos för mobil. Ny linkedin ikon i footer.','2016-03-08 15:43:34',NULL),
	(213,18,46,1,0.15,'AdjustHeight på lightslider','2016-03-15 08:09:46',NULL),
	(214,21,47,1,0.30,'Möte med EPS på TH.','2016-03-21 09:44:59',NULL),
	(215,21,47,1,6.00,'Designar med Anna Maria på TH.','2016-03-22 09:45:46',NULL),
	(216,21,47,1,5.00,'Designskisser batch 1, Anna Maria mailar dessa till kund för översyn.','2016-03-23 09:47:06',NULL),
	(217,21,47,1,6.00,'Animationer för Benefits, ikoner för sidan Technical.','2016-03-24 18:59:24',NULL),
	(218,21,47,1,2.00,'Skapar kartan + nya varianter på animationer.','2016-03-25 13:03:47',NULL),
	(219,21,47,1,7.00,'Klipper bilder, animerar, kalkylatorn design.','2016-03-29 19:22:18',NULL),
	(220,21,47,1,7.00,'Redigerar bilder, kartikon (liten), fixar eps logo m.m.','2016-03-30 19:23:00',NULL),
	(221,1,48,1,0.30,'Lägger upp ny bild för: På tillfälligt besök. typ: Inlägg - wp-postID:632 (Fika och annat kul)','2016-03-30 19:28:05',NULL),
	(222,21,47,1,2.30,'Konverterar pdf till jpg. Justerar animationer för Benefits.','2016-03-31 11:09:00',NULL),
	(223,21,47,1,1.00,'Klipper technical docs bilder.','2016-03-31 15:51:02',NULL),
	(224,21,49,1,0.30,'Klipper om bilder till större storlek.','2016-04-04 08:40:28',NULL),
	(225,21,49,1,6.30,'Redigerar grafik, animerar','2016-04-05 11:25:19',NULL),
	(226,21,49,1,2.00,'CO2 animation, klipper ut jordglob m.m','2016-04-06 11:26:02',NULL),
	(227,22,50,1,3.00,'Skissar på logotyp.','2016-04-04 11:28:38',NULL),
	(228,22,50,1,4.00,'Renderar PGX sigill + logotypförslag.','2016-04-05 11:29:08',NULL),
	(229,22,50,1,4.00,'Skapar några nya logoförslag till PGX med lampa / sol.','2016-04-07 13:31:19',NULL),
	(230,22,50,1,2.00,'Möte med PG. Nytt legonamn PGX-Flow Production. Skapar en skiss på den.','2016-04-11 08:52:53',NULL),
	(231,23,51,1,3.50,'Mockup för startsida (header) med lite mer äventyrligt tema.','2016-04-11 08:54:23',NULL),
	(232,23,51,1,5.00,'Skissar på citygolf mitten och footer.  Gör 2 alternativ till Tomas Rimbark vidare till kunden.','2016-04-12 15:21:45',NULL),
	(233,21,49,1,0.15,'Klipper ny bild till startsidan.','2016-04-12 15:22:13',NULL),
	(234,21,49,1,0.50,'Klipper till film för startsidan.','2016-04-13 22:18:01',NULL),
	(235,22,50,1,1.00,'Skissar på PGX Flow logo.','2016-04-12 22:19:12',NULL),
	(236,1,48,1,1.00,'Lägger upp ny bild för produkt, fixar så mer än 10 produkter listas, fixar in sortering av produkter.','2016-04-13 22:20:17',NULL),
	(237,24,52,1,6.00,'Skapar första utkast på musik inspirerat av Tobias förslag.','2016-04-13 22:23:53',NULL),
	(238,21,49,1,0.30,'Lägger in Zambia o Zimbabwe igen.','2016-04-14 11:13:03',NULL),
	(239,23,51,1,0.30,'Telefonmöte med CityGolf. CG ska återkomma med eget designförslag då dom inte kan enas.','2016-04-14 15:12:04',NULL),
	(240,24,52,1,5.00,'Gör 2 andra utkast på meny-musik till BMX 2.','2016-04-15 11:23:12',NULL),
	(241,22,50,1,1.00,'Skapar variant på PGX Flow logo (med plus / minus)','2016-04-18 11:23:35',NULL),
	(242,12,53,1,6.00,'Blender - Tåg modellerar och textureror.','2016-04-18 11:39:56',NULL),
	(243,24,52,1,0.20,'Telefonmöte med Tobias, gillar bmx_01.mp3. Testa byta gitarrer mot annat instrument. Sopranos? Ej Collage pop sounds.','2016-04-19 11:40:45',NULL),
	(244,24,52,1,4.00,'Remix på BMX 01, gör synth plucks, hittar en ny lead.','2016-04-20 09:37:16',NULL),
	(245,24,52,1,2.00,'Remix på BMX 01, återgår till gamla introt. Mer fokus på basens riff och ljud. Skickar till Tobias.','2016-04-22 18:47:58',NULL),
	(246,22,50,1,6.00,'Rendering till startsidan (city) för produkter.','2016-04-22 18:48:49',NULL),
	(247,21,49,1,0.30,'Skapar dokument för grafisk profil med färgkoder m.m.','2016-04-21 18:49:27',NULL),
	(248,18,46,1,0.30,'Uppdaterar Custom Fields plugins pga javascript bugg iom. senaste wordpress.','2016-04-26 18:11:00',NULL),
	(249,22,55,1,4.00,'Design - produktsida v1, skapar resten av startsidan, lite ikoner, footer m.m.','2016-04-27 16:23:53',NULL),
	(250,24,56,1,4.00,'Ljudlägget bmx 2 filmklipp och skickar till Tobias.','2016-04-28 16:27:47',NULL),
	(251,22,55,1,4.00,'Skapar hus-illustration för markkollektor, påvisa hur mycket mindre man behöver gräva.','2016-04-29 15:20:18',NULL),
	(252,20,57,1,3.00,'VO redigering och ljuddesign på Nya ledare Jonas.','2016-05-01 13:50:57',NULL),
	(253,22,55,1,1.00,'Möte med PG ang hemsidan. Går igenom google drive.','2016-05-02 14:56:03',NULL),
	(254,20,57,1,2.00,'Ljuddesign på Nya Ledare Jonas.','2016-05-02 10:11:09',NULL),
	(255,20,57,1,1.00,'Mixar Nya Ledare Jonas och skickar till Soja.','2016-05-03 10:11:27',NULL),
	(256,22,55,1,3.00,'Produktsidan, justerar house_01 3D, lägger in Documents, förslag på textblock i google docs. Förslag på bild på flythus. Skickar till PG.','2016-05-04 16:18:03',NULL),
	(257,22,55,1,5.00,'Sätter upp wordpress lokalt. Skapar skyline och jobbar med fullpage.','2016-05-09 09:38:24',NULL),
	(258,22,55,1,1.30,'Block nr 2 (kundtyper) på Home.','2016-05-10 11:28:03',NULL),
	(259,25,58,1,2.00,'Menyskisser, 2 varianter.','2016-05-10 11:30:00',NULL),
	(260,22,55,1,1.30,'PGX WP Home - block 2 och 3 (Subscribe).','2016-05-10 14:21:45',NULL),
	(261,22,55,1,4.00,'Filmclip animation test of skyline.','2016-05-11 08:40:20',NULL),
	(262,22,55,1,6.00,'Gör klart footer css. Skapar About Us skiss och skickar PG.','2016-05-12 15:44:05',NULL),
	(263,22,55,1,5.00,'Jobbar på header. Hamburger icon and animation.','2016-05-13 08:59:41',NULL),
	(264,26,59,1,0.30,'Möte med Andreas Lindahl ang Norra Skogsägarna, ny webbprofil.','2016-05-13 09:38:08',NULL),
	(265,26,59,1,0.30,'Läser igenom värdegrund: Hur skogen fungerar','2016-05-16 09:38:34',NULL),
	(266,22,55,1,3.30,'Fixar menyfärger och minskar logo när man scrollar. Fixar position på produkter i skyline.','2016-05-16 11:13:25',NULL),
	(267,22,55,1,2.30,'Climate Ceiling link, special admin header css so it looks good in wp admin dashboard.','2016-05-16 15:27:03',NULL),
	(268,27,60,1,4.00,'Skapar draft på Yaga Menu music.','2016-05-16 11:41:57',NULL),
	(269,26,59,1,2.00,'Letar bilder, påbörjar mockup för startsida.','2016-05-17 11:28:11',NULL),
	(270,27,60,1,10.00,'Draft: Exploration.','2016-05-18 11:28:45',NULL),
	(271,20,57,1,2.00,'Ljudlägger Hans Lindberg och skickar till Jakob på Soja.','2016-05-20 13:11:59',NULL),
	(272,27,60,1,9.00,'Draft: Combat','2016-05-19 13:12:15',NULL),
	(273,26,59,1,8.00,'Designar på skisser för Norra.','2016-05-23 09:55:01',NULL),
	(274,27,60,1,6.00,'Yaga Combat 02 cue.','2016-05-22 09:55:39',NULL),
	(275,18,46,1,1.30,'Varningar i manual. Skickar till Tobias.','2016-05-24 12:06:47',NULL),
	(276,18,46,1,1.00,'Facebook och Google Plus ikon.','2016-05-24 12:07:00',NULL),
	(277,27,60,1,3.00,'Mixar klart Combat 02_2','2016-05-24 08:09:37',NULL),
	(278,22,55,1,2.00,'Möte med PG på Västerslätt ang. hemsidan / filmning av poolvärmare m.m.','2016-05-25 17:55:01',NULL),
	(279,26,59,1,2.00,'Redigerar startsidan med olika bakgrunder.','2016-05-25 21:21:57',NULL),
	(280,26,59,1,3.00,'Letar bilder','2016-05-24 10:00:48',NULL),
	(281,26,59,1,1.30,'Presentation av Norra Webb designförslag för TH.','2016-05-26 10:01:27',NULL),
	(282,26,59,1,2.30,'Flyttar ner logo från header till bild på N Timber. Testar olika bakgrunder.','2016-05-26 12:34:10',NULL),
	(283,18,46,1,0.30,'Fixar Guides så de fungerar även vid Sök.','2016-05-29 20:00:58',NULL),
	(284,18,46,1,4.00,'Guides på supportsidan.','2016-05-26 20:05:21',NULL),
	(285,27,60,1,7.00,'Re-arranging Yaga Menu music.','2016-05-27 08:53:09',NULL),
	(286,27,60,1,2.00,'Mixar klart Yaga Menu 1-2.','2016-05-28 08:53:25',NULL),
	(287,26,59,1,4.00,'Skissar på footer, testar nya bilder på startsida och skapar footer. Expanderar media delen med video.','2016-05-30 14:00:28',NULL),
	(288,26,59,1,0.30,'Möte med Andreas Lindahl, går igenom nya Norra skisser.','2016-05-31 11:57:16',NULL),
	(289,26,59,1,2.00,'Flyttar logo till vänster på desk och mobil. Gör meny för Om Oss, skissar på sida Vision m.m.','2016-05-31 11:58:10',NULL),
	(290,22,61,1,6.00,'Blender - Flythus. Redigerar skyline vyn. Fixar CSS för fullpage. About Us sidan.','2016-06-01 11:26:24',NULL),
	(291,22,61,1,3.00,'Skyline CSS fullpage iPad fix height. Människor i Skyline. Ljusjusterar bilden.','2016-06-02 11:27:07',NULL),
	(292,26,59,1,1.00,'Tittar på gamla skisser, funderar vilken typ av meny som fungerar. Tittar på svenska kyrkan. Mailar Andreas. Skapar nya utkast på mobilmeny för Timber.','2016-06-02 13:37:51',NULL),
	(293,22,61,1,2.00,'Renderar om bakgrund till Subscribe. Fixar CSS på About US sidan.','2016-06-07 11:28:36',NULL),
	(294,22,61,1,2.00,'CSS på About Us sidan. Fixar så herobilden fyller ut höjden på skärmen på iOS..','2016-06-08 11:29:13',NULL),
	(295,26,59,1,2.00,'Testar lägga in en flicka med planta på startbilden.','2016-06-08 11:31:36',NULL),
	(296,28,62,1,8.00,'Menymusik utkast 1','2016-06-03 11:33:22',NULL),
	(297,28,62,1,3.00,'Justerar menymusik och skickar till Yuval.','2016-06-05 11:33:47',NULL),
	(298,1,63,1,1.00,'Lägger in sos barnbyar banner i footer.','2016-06-09 14:59:52',NULL),
	(299,22,61,1,2.00,'Fixar så menyn är låst på alla sidor utom startsidan som ska ha ren meny och större logo. Påbörjar Products sidan, lägger in custom types.','2016-06-12 21:24:06',NULL),
	(300,26,59,1,1.00,'Möte med Andreas Lindahl och Susanne på TH.','2016-06-13 09:57:41',NULL),
	(301,22,61,1,7.00,'Fixar produktsidorna, färgtema på produkter, skapar services & projects template. Fixar dokumentlänkar.','2016-06-14 16:21:59',NULL),
	(302,26,59,1,2.00,'Justerar mediablocket så det blir snyggare marginaler och flyttar in TAG i bilden (Nyheter, Video etc.)','2016-06-14 13:41:05',NULL),
	(303,26,59,1,2.00,'Presentation av Norra Design för Erik och Tobias.','2016-06-15 13:41:35',NULL),
	(304,6,28,1,2.00,'Lägger in ny iOS användare Torbjörn och skickar ut via TestFairy. ','2016-06-15 13:49:18',NULL),
	(305,29,64,1,2.00,'Skissar på storyboard på iPad.','2016-06-28 10:29:51',NULL),
	(306,30,65,1,4.00,'Create Searching Opponent cue','2016-07-05 12:04:26',NULL),
	(307,30,66,1,4.00,'Create Roulette spinning ball and ball stopping sounds.','2016-07-06 12:05:06',NULL),
	(308,26,67,1,8.00,'Home Revision, full logo in top, extend menu with titles (categories like Ekonomiska tjänster). Create 3 underpage demos.','2016-08-08 11:18:22',NULL),
	(309,26,67,1,1.00,'Design meeting with Andreas Lindahl','2016-08-09 11:18:52',NULL),
	(310,29,64,1,8.00,'Skissar på tallar, animerar i Blender.','2016-08-15 21:47:55',NULL),
	(311,29,64,1,8.00,'Animerar kretsloppet i blender','2016-08-12 21:48:16',NULL),
	(312,29,64,1,4.00,'Animerar kretsloppet i Blender.','2016-08-11 21:48:46',NULL),
	(313,27,68,1,3.00,'Trailer music mockup, skype with Catelin. Await further instructions since they might license music from a band.','2016-08-11 21:54:23',NULL),
	(314,2,69,1,1.00,'Justerar omsättning och procent på sidan Om Oss->Företaget.','2016-08-17 21:56:02',NULL),
	(315,31,70,1,0.15,'Lägger till 2016,2017,2018 i E2 logo.','2016-08-23 08:36:56',NULL),
	(316,32,71,1,4.00,'Skapar ljud till daily bonus. Klipper ut ljud åt Asaf / Yoni på C2P.','2016-08-22 08:39:08',NULL),
	(317,29,64,1,8.00,'Ritat nya skisser på träd till animation.','2016-08-22 08:39:57',NULL),
	(318,29,64,1,4.00,'Justerar animation och byter ut till nya tallar, lägger in moln och gran i bakgrunden, Ljudlägger.','2016-08-23 08:40:25',NULL),
	(319,29,64,1,0.50,'Möte Erika','2016-08-09 10:02:19',NULL),
	(320,29,64,1,0.45,'Möte Erika, genomgång ny film, nya illustrationer.','2016-08-24 10:03:09',NULL),
	(321,29,64,1,2.00,'Revision av animation, blå bakgrund, bättre skylt, skarpare film.','2016-08-24 21:34:47',NULL),
	(322,26,72,1,1.00,'Möte på Norra med Erik och Andreas.L. Presenterar exempel på innehållssidor.','2016-08-30 10:09:49',NULL),
	(323,33,73,1,8.00,'Tittar på idéer för vinjetten i Blender.','2016-08-29 10:12:36',NULL),
	(324,33,73,1,0.30,'Inledande möte ang vinjetten m.m.','2016-08-24 10:13:32',NULL),
	(325,1,74,1,0.15,'Lägger in cake hacks på startsidan, längst ner. Se google doc för mer referens på page id.','2016-08-30 20:40:49',NULL),
	(326,33,73,1,8.00,'Renderar olika klipp för vinjetten. Skog som animeras fram. Tree transition.','2016-08-31 20:42:09',NULL),
	(327,33,73,1,8.00,'Skylt animation, tar foto på skrivbord. Testar göra musik.','2016-09-01 20:42:36',NULL),
	(328,33,73,1,9.00,'Skapar utkast på blå / grön namnskylt. Renderar varianter på musik. Finjustera inför presentation imorgon.','2016-09-05 22:54:02',NULL),
	(329,33,73,1,4.00,'Växande träd i namnskylten. Ny bakgrundsbild med tall som växer i gran.','2016-09-06 13:22:59',NULL),
	(330,33,73,1,1.00,'Möte med Erika. Presenterar vinjett, går igenom kretsloppet ny copy m.m','2016-09-06 13:23:30',NULL),
	(331,34,75,1,2.00,'Pin Coin sound design on video clip.','2016-09-06 15:15:34',NULL),
	(332,34,75,1,0.30,'Many bumper-pin sounds, random pitch / pan.','2016-09-07 15:15:56',NULL),
	(333,33,76,1,6.00,'Fixar grön folder i slutet, fixar textruta.','2016-09-08 15:17:18',NULL),
	(334,29,77,1,3.00,'Arbetar om copy. Skriver avslut.','2016-09-08 10:06:40',NULL),
	(335,29,77,1,6.00,'Animerar markberedare, partikeltest.','2016-09-12 09:30:40',NULL),
	(336,35,78,1,1.00,'Möte med Andreas. Genomgång av ny webb.','2016-09-13 13:14:28',NULL),
	(337,11,79,1,3.00,'Gör custom fields sökbara (produktnummer etc.), lägger in Pressure, Temperature, Rotational direction, rotational speed på product specifications.','2016-09-14 08:02:41',NULL),
	(338,26,72,1,1.00,'Presentation av Norra webbplatser hos Norra.','2016-09-13 08:03:27',NULL),
	(339,33,76,1,8.00,'Redigerar bort STIHL m.m. Renderar ut clip i 1920x1080. Lägger in ny wilma bild.','2016-09-14 08:04:56',NULL),
	(340,29,77,1,8.00,'Spelar in ny voice over enligt copy v3.','2016-09-15 11:42:38',NULL),
	(341,33,76,1,6.00,'Skapar / klipper ut avslut för båda vinjetterna.','2016-09-16 11:43:20',NULL),
	(342,33,80,1,2.00,'Testar spela in röst för Klimatfrågan del 1.','2016-09-15 11:46:04',NULL),
	(343,34,81,1,3.00,'Mega Bonus Sounds','2016-09-16 15:01:25',NULL),
	(344,33,76,1,4.00,'Renderar alla klipp på PC och exporterar som Quicktime - CINEPRO. Klipper till namnskyltar och gör demo med text.','2016-09-18 23:09:32',NULL),
	(345,35,78,1,10.00,'Påbörjar första utkast på skisser för Vitec.','2016-09-19 09:30:46',NULL),
	(346,35,78,1,7.00,'Vitec design och genomgång med Andreas, Petter o Karin.','2016-09-20 09:31:14',NULL),
	(347,33,76,1,1.00,'Leverans av vinjett till Ulrica och Erika på Skogsskolan','2016-09-19 09:32:19',NULL),
	(348,35,78,1,8.00,'Vice design','2016-09-21 14:17:16',NULL),
	(349,35,78,1,6.00,'Vitec design, möte med Karin och Petter.','2016-09-22 08:59:26',NULL),
	(350,11,82,1,1.00,'Lägger in prod-spec: Power Socket och Power Frequency.','2016-09-23 09:41:10',NULL),
	(351,35,78,1,2.30,'Möte med Vitec, pers av skisser.','2016-09-23 08:28:48',NULL),
	(352,29,83,1,8.00,'Redigerar om timeline enligt ny VO fil (ver 3), lägger in björkar m.m.','2016-09-26 08:27:30',NULL),
	(353,35,84,1,10.00,'Skissar på com sida, lägger in ram runt sidan.','2016-09-26 00:00:19',NULL),
	(354,35,84,1,10.00,'Telmöte med Catarina på Vitec, justerar .com enligt dialog (ABB).','2016-09-27 00:01:10',NULL),
	(355,36,85,1,3.00,'Skapar 4 sidor med utkast på logotyp. Rev 1.0.','2016-09-28 14:53:18',NULL),
	(356,36,85,1,1.00,'Gör utkast 05 och skickar Agnetha.','2016-09-29 10:41:01',NULL),
	(357,35,84,1,4.00,'Ny skiss på Marknad Sverige med Supportbox (05.ao)','2016-09-28 10:42:22',NULL),
	(358,35,84,1,3.00,'Skiss på vanlig sida med massa text, skickar till Catarina.','2016-09-29 20:56:40',NULL),
	(359,36,85,1,2.00,'Jobbar med färg på logotypen.','2016-09-29 20:56:54',NULL),
	(360,35,84,1,3.00,'Tar fram skiss för desktop, laptop, tablet och phone. Skickar till Catarina.','2016-09-30 10:42:47',NULL),
	(361,35,84,1,5.00,'Skissar enligt nya förslag från Catarina pptx.','2016-09-30 11:00:00',NULL),
	(362,35,84,1,7.00,'Skissar på AO sverige och AO FI. Kontaktsida. Flera branschluffar.','2016-10-03 08:40:35',NULL),
	(363,35,84,1,5.00,'Skissar på Kontaktsida, sök, möte med Karin m.m.','2016-10-04 13:47:10',NULL),
	(364,35,84,1,4.00,'Telemöte med Karin / Vitec. Skissar på Pressmeddelanden, AO start, search result','2016-10-05 10:55:38',NULL),
	(365,35,84,1,4.00,'Skissar på produktsida Aiolos. Kontaktsida. Meny och navigering.','2016-10-06 09:29:52',NULL),
	(366,35,84,1,1.30,'Meny och navigering, mailar Catarina batch 5.','2016-10-07 09:30:17',NULL),
	(367,36,85,1,1.00,'Exporterar Oqurenz logo i alla format och levererar till kund.','2016-10-06 09:30:59',NULL),
	(368,17,86,1,4.00,'Laddar hem och spelar Aqua Moto Utopia på steam. Behövs mer vattensplashar.','2016-10-05 15:09:00',NULL),
	(369,17,86,1,4.00,'Skapar 31st hull_bumps och laddar upp på zordix ftp.','2016-10-07 15:09:19',NULL),
	(370,35,84,1,2.00,'Skapar Rapportlista med pdf.','2016-10-10 09:45:25',NULL),
	(371,29,83,1,8.00,'Talavstämning med Erika. Klipper in karaktären. Bone-structure inför animering.','2016-10-10 21:02:18',NULL),
	(372,35,84,1,3.00,'Skissar Formulär, bredare footer. Mejlar Karin mfl.','2016-10-11 10:45:50',NULL),
	(373,29,83,1,6.00,'Walk cycle, saw rotate. Eye blinks.','2016-10-11 22:25:48',NULL),
	(374,17,86,1,2.00,'Spelar in nytt filmklipp från Aqua på PC.','2016-10-10 22:27:55',NULL),
	(375,17,86,1,4.00,'Ljudlägger menyfilm, nya splashar och vattensplatter, måsljud, vindsus, nya knappljud.','2016-10-12 13:22:07',NULL),
	(376,11,87,1,1.00,'Fixar så splash roterar per delay som man ställer i wp settings.','2016-10-12 13:56:24',NULL),
	(377,29,83,1,7.00,'Animerar walk, lift saw, kapa tre björkar.','2016-10-12 08:26:04',NULL),
	(378,11,87,1,0.30,'Låser så splash ej roterar om det bara finns en splash. Lägger live.','2016-10-13 09:07:34',NULL),
	(379,36,85,1,1.00,'Revision av logo, grön + svart. Skickar ny batch.','2016-10-13 14:18:10',NULL),
	(380,17,88,1,2.00,'Mailar VO nissar, räknar ord i listan ca 1600 ord.','2016-10-13 08:39:59',NULL),
	(381,11,87,1,1.00,'Justerar manuallistan på en produkt både i single page view och produktlistan.','2016-10-14 09:39:16',NULL),
	(382,29,83,1,4.00,'Sågspån vid kapning av alla träd.','2016-10-13 19:33:42',NULL),
	(383,29,83,1,4.00,'Ljudeffekter och renderar ut test av nuvarande sekvens.','2016-10-14 19:34:03',NULL),
	(384,26,89,1,1.30,'Fotomöte på Norra med Ulrica, Andreas, Kalle fotograf.','2016-10-17 14:25:27',NULL),
	(385,24,90,1,4.00,'Revision på knapp, start vo och tuta, hoppljud, spinnljud m.m.','2016-10-17 14:27:10',NULL),
	(386,35,84,1,0.30,'Innehållssida.','2016-10-17 14:59:39',NULL),
	(387,33,80,1,8.00,'Påbörjar storyboard i Blender.','2016-10-18 08:53:08',NULL),
	(388,11,87,1,0.30,'Felsöker på databas connection error = Binero som kört mysql uppgradering.','2016-10-19 08:54:17',NULL),
	(389,24,90,1,0.30,'Mailar Peter o Tobias om VO.','2016-10-18 08:54:53',NULL),
	(390,17,86,1,3.00,'Klipper nya menyljuden och laddar upp på ftpn. Mailar Matti om leveransen.','2016-10-19 10:25:32',NULL),
	(391,33,80,1,1.00,'Storyboard på klossar som faller.','2016-10-19 10:26:18',NULL),
	(392,26,89,1,1.00,'Norra möte ang. Kontaktsida med Andreas L och Magnus på TH.','2016-10-19 23:13:48',NULL),
	(393,33,80,1,3.00,'Storyboardar klart del 1 av sida 1.','2016-10-19 23:14:24',NULL),
	(394,33,80,1,1.00,'Justerar och renderar ut nytt klipp.','2016-10-20 17:37:19',NULL),
	(395,24,90,1,4.00,'Spelar in cykel som droppar från ramp på baksidan. Lägger ljud på nytt filmklipp 01.','2016-10-24 08:01:53',NULL),
	(396,29,91,1,3.00,'Skapar svampar, justerar höjd på markberedare, Björkar växer.','2016-10-24 08:04:02',NULL),
	(397,17,86,1,8.00,'Skapar musiktema för Neon City - Shanghai. utkast 01.','2016-10-24 08:04:33',NULL),
	(398,17,86,1,14.00,'Skapar musik för Oil Rig level.','2016-10-20 08:05:32',NULL),
	(399,26,92,1,5.00,'Skapar kontaktsida, 2 varianter.','2016-10-25 10:59:17',NULL),
	(400,24,90,1,4.00,'Lägger in ny VO. interfaceljud, grus vid upp / ned, nya vindar.','2016-10-25 10:59:55',NULL),
	(401,24,90,1,1.00,'Ny starkare vind','2016-10-26 11:00:13',NULL),
	(402,35,93,1,1.00,'Exporterar .psd, skriver in marginaler och skickar till Per Sturesson.','2016-10-26 15:59:21',NULL),
	(403,26,92,1,6.00,'Kontaktsida, expandera Huvudkontor. Styrelse och VD vy med personal.','2016-10-26 16:00:05',NULL),
	(404,17,86,1,5.00,'Letar ljudbild för Shanghaitema.','2016-10-26 10:51:16',NULL),
	(405,29,91,1,8.00,'Ritar myr, blockmark, vattenskyddszon. Korrigerar röjningsanimation.','2016-10-27 08:24:49',NULL),
	(406,26,92,1,3.00,'Skapar extra utkast för supmeny på kontaktsida.','2016-10-28 10:58:38',NULL),
	(407,2,94,1,0.30,'Felsöker varför det inte går att ändra på produkter. Mejlar Johan att det krävs djupare felsökning.','2016-10-28 11:00:07',NULL),
	(408,26,92,1,1.00,'Kontakt_sidebar_3','2016-10-28 11:23:41',NULL),
	(409,29,91,1,6.00,'Gräs i förgrund. Fixar så molnen syns. Renderar ut nytt utkast 04.','2016-10-28 21:41:59',NULL),
	(410,29,91,1,8.00,'Påbörjar scen 2, gör skiss på gallringstraktor med träd. utzoomat.','2016-10-31 13:49:35',NULL),
	(411,29,91,1,6.00,'Skapar traktor som kör, skapar träd för scen 2.','2016-11-01 08:47:56',NULL),
	(412,26,92,1,8.00,'Skapar kontaktpuff, sökruta, sökresultat, avancerad sök. Skickar till Andreas.L.','2016-11-02 09:13:22',NULL),
	(413,24,90,1,7.00,'Skapar race_over_02 musik och fixar bort bubbelljud från race_win.','2016-11-04 09:08:44',NULL),
	(414,37,96,1,4.00,'Spelar in ny voice over enligt justerat manus från Erika.','2016-11-07 13:27:29',NULL),
	(415,24,90,1,6.00,'Mer overdrive på announcer. Klipper ut ljud och skickar till Turborilla.','2016-11-07 09:06:20',NULL),
	(416,26,92,1,7.00,'Möte med Andreas och David på TH. Tar bort submeny på kontaktsidan. Jag ska göra mobilskisser och skicka färgkoder m.m.','2016-11-08 09:08:02',NULL),
	(417,26,92,1,1.30,'Gör klart vanlig sida och sidebar puff. Skriver ner färgkoder','2016-11-09 09:10:13',NULL),
	(418,29,91,1,6.00,'Animerar björkar som växer, kamera som zoomar in. Traktor som pekar på ett böjt träd.','2016-11-09 08:24:48',NULL),
	(419,29,91,1,2.00,'Svampar, grenar o stenar i Gallringsscenen. Fällning av träd 2.','2016-11-10 23:23:52',NULL),
	(420,26,92,1,3.00,'Marginaler för mobilmeny.  Påbörjar nyhetslista.','2016-11-10 23:24:47',NULL),
	(421,26,92,1,4.00,'Nyhetsarkiv, nyhetslista.','2016-11-14 11:43:32',NULL),
	(422,29,91,1,5.00,'Lägger in skyltar, animerar 90-100 år skog som växer och kapas ner. Testar rendera hela filmen i scen 3.','2016-11-15 14:10:47',NULL),
	(423,29,91,1,3.00,'Testar göra slutscenen med alla faser i Kretsloppet.','2016-11-15 09:35:01',NULL),
	(424,29,91,1,6.00,'Skapar musik för slutscenen.','2016-11-16 10:01:27',NULL),
	(425,24,90,1,4.00,'Desertlevel, Fall sounds.','2016-11-17 00:34:00',NULL),
	(426,11,95,1,1.00,'Skapar ikon för prodkat - Consumables (cartridge)','2016-11-21 11:27:28',NULL),
	(427,24,90,1,1.00,'Tankar hem ny build på BMX 2 och testspelar 8 nivåer. Låser upp ny skogsbana.','2016-11-22 10:37:11',NULL),
	(428,37,96,1,4.00,'Påbörjar ny storyboard enligt nytt manus.','2016-11-21 10:41:01',NULL),
	(429,29,91,1,3.00,'Lägger in skyltar i början och lägger in mer buskage. Justerar slutscenen.','2016-11-21 10:42:01',NULL),
	(430,29,91,1,3.00,'Renderar ut ny demo på hela Kretsloppsfilmen.','2016-11-22 10:42:25',NULL),
	(431,2,94,1,3.50,'Besöker Nicklas. Uppdaterar Toolset plugin och autentiserar denna. Testkör lokalt och uppdaterar live.','2016-11-21 11:15:36',NULL),
	(432,24,97,1,2.00,'Kraschljud, body_impact.','2016-11-23 08:35:38',NULL),
	(433,29,98,1,2.00,'Skissar på Skotare (maskin som plockar timmer)','2016-11-23 08:36:45',NULL),
	(434,37,99,1,1.00,'Möte med Erika ang. filmerna och fortsatt arbete. Nybeställning - Certifikat.','2016-11-24 22:32:01',NULL),
	(435,26,100,1,2.00,'Möte med Andreas Lindahl, summerar Norra design i katalogstruktur. Påbörjar kartan för virkesområden.','2016-11-25 22:33:46',NULL),
	(436,26,100,1,2.00,'Skissar varianter på virkesområden-kartan.','2016-11-28 10:43:21',NULL),
	(437,24,97,1,4.00,'Ljuddesign menyljud, swipes, byta cykel m.m.','2016-11-28 08:56:45',NULL),
	(438,26,100,1,2.00,'Designar om kartan, planar ut så det ser ut som Sverige. testar färger och tar bort namnen på områden, skalar bort.','2016-11-28 08:57:25',NULL),
	(439,24,97,1,5.00,'Klipper ut ljud för unlock new level, race finished, race lost m.m.','2016-11-29 11:15:14',NULL),
	(440,2,94,1,1.00,'Uppdaterar språkplugin och fixar css för flaggor.','2016-12-01 11:40:32',NULL),
	(441,26,100,1,2.00,'Exporterar karta och pilar som svg och testar detta på en sida. Mejlar Andreas.','2016-12-02 13:01:18',NULL),
	(442,37,99,1,2.00,'Skissar på storyboard.','2016-12-02 21:15:17',NULL),
	(443,37,99,1,3.00,'Skissar på storyboard för scenen med sverigekartan och landsbygden.','2016-12-05 23:09:37',NULL),
	(444,29,98,1,3.00,'Modellerar Skotaren som ska plocka timmer.','2016-12-05 23:10:00',NULL),
	(445,29,98,1,8.00,'Animerar skotare som plockar upp stockarna. Lägger in lite mer ljud.','2016-12-07 08:58:09',NULL),
	(446,24,97,1,4.00,'Beställer VO för female rider, redigerar dessa.','2016-12-05 08:59:42',NULL),
	(447,11,95,1,0.30,'Skickar manualen till Tobias via Sprend.','2016-12-08 11:38:30',NULL),
	(448,29,98,1,8.00,'Stort träd i scen 1. Fixar ny ikon för vattenskyddsområden.','2016-12-08 23:52:35',NULL),
	(449,29,98,1,5.00,'Lägger in intro och outro i kretsloppsfilmen.','2016-12-09 23:42:06',NULL),
	(450,37,99,1,4.00,'Påbörjar storyboard för skogen som ersättning för olja i produkter m.m.','2016-12-12 23:14:11',NULL),
	(451,29,98,1,4.00,'Ljudlägger hela Kretsloppsfilmen korrekt i Logic och justerar transitions mellan klippen.','2016-12-12 23:14:55',NULL),
	(452,29,98,1,3.00,'Justerar sten som försvann. Fixar transition från intro till kretsloppsfilmen,','2016-12-13 15:35:00',NULL),
	(453,37,99,1,4.00,'Skissar på fisk som kommer fram bakom lådan och plast som försvinner.','2016-12-13 15:35:27',NULL),
	(454,24,97,1,3.00,'Skapar Stadium, Junkyard och Urban City ambience + längre start drop.','2016-12-15 22:05:16',NULL),
	(455,37,99,1,4.00,'Skissar på solenergi, botemedel mot cancer och export (lastbil - flaggor).','2016-12-16 23:59:55',NULL),
	(456,37,99,1,4.00,'Skissar på sista scenerna i filmen.','2016-12-20 00:49:10',NULL),
	(457,37,99,1,6.00,'Förfinar de sista scenerna och klipper ihop det till en film. Dropboxar.','2016-12-20 19:51:43',NULL),
	(458,37,102,1,8.00,'Skissar på storyboard. Påbörjar scen 1 med vågen och boxarna.','2017-01-10 10:00:13',NULL),
	(459,37,102,1,8.00,'Gör klart scen 1 och skissar på grafik för scen 2 och väljer färger.','2017-01-11 10:00:45',NULL),
	(460,37,102,1,8.00,'Pengar som flyger upp ur båten och lådornas fall och övergången från våg till båtscen.','2017-01-12 23:33:26',NULL),
	(461,37,102,1,7.00,'Skapar ny ikon för transportsystem (spårvagn) ljudlägger.','2017-01-13 09:45:46',NULL),
	(462,38,103,1,8.00,'Påbörjar storyboard','2017-01-16 08:00:00',NULL),
	(463,38,103,1,8.00,'Skissar på sc1-1 till 1-3 med obrukad skog.','2017-01-17 12:51:37',NULL),
	(464,38,103,1,6.00,'Klimatnyttan skissar klart nr 7,8,9,10 och mejlar Erika.','2017-01-18 21:23:45',NULL),
	(465,24,104,1,7.00,'New effects according to batch 4-order. Shine, 20 new bmx impacts, new whooping sound.','2017-01-19 11:42:27',NULL),
	(466,24,104,1,3.00,'New whooping with freespin sounds. 2 variations.','2017-01-20 11:43:07',NULL),
	(467,24,104,1,1.00,'Skapar nytt videoklipp med mindre grusljud, klipper ut och loopar whooping sounds.','2017-01-20 08:30:16',NULL),
	(468,38,103,1,7.00,'Clean storyboard, soon ready for new delivery.','2017-01-23 23:07:32',NULL),
	(469,24,104,1,2.00,'Skapar flygande sedlar och money tick.','2017-01-23 23:07:59',NULL),
	(470,38,103,1,2.00,'Research klimatnyttan','2017-01-18 09:48:46',NULL),
	(471,24,107,1,3.00,'Flying bills, new load screen, new bike sounds.','2017-01-24 20:32:46',NULL),
	(472,24,107,1,4.00,'Level Up, pause sound.','2017-01-25 20:33:35',NULL),
	(473,38,106,1,4.00,'Skissar på klimatnyttan storyboard.','2017-01-24 20:34:05',NULL),
	(474,24,107,1,1.00,'Rocket boost','2017-01-25 21:58:35',NULL),
	(475,24,107,1,8.00,'Gör i ordning underlag för voiceover artister och beställer vo, spelar in Chuck (shrimp), skapar tut-fail / ok, descend effekt.','2017-01-26 10:05:55',NULL),
	(476,24,107,1,5.00,'Klipper Shrimp och Cain VO. Skapar notification och Turborilla_cue.','2017-01-27 14:50:44',NULL),
	(477,24,107,1,3.00,'Gör deal och testklipper Rastafari. Klipper Loco.','2017-01-30 09:59:09',NULL),
	(478,24,107,1,4.00,'Beställer female rastafari, skapar nya win videos och crowd-applaus + klipper om win musiken.','2017-01-31 21:48:17',NULL),
	(479,29,108,1,3.00,'Skapar kretsloppsbild.','2017-01-31 21:48:43',NULL),
	(480,24,107,1,3.00,'Notifieringsvariationer.','2017-02-01 11:19:04',NULL),
	(481,39,109,1,1.00,'Möte med Susanne Lindquist ang. design för Lipum. Tittar på 2 bilder, en stillbild och en process som visar hur inflammationen blir till / hämmas.','2017-02-02 12:39:14',NULL),
	(482,29,108,1,3.00,'Krestloppsbild_02, flöde.','2017-02-03 14:18:20',NULL),
	(483,24,107,1,1.00,'Cuts Jamaican female voice over.','2017-02-06 11:51:36',NULL),
	(484,11,110,1,4.00,'Image at bottom of home (clickable svg), new product category','2017-02-02 23:11:18',NULL),
	(485,11,110,1,3.00,'Product image gallery with Advanced Custom Fields Pro.','2017-02-03 23:11:56',NULL),
	(486,11,110,1,1.00,'Gör klart product image gallery så bilder fejdar snyggt och nav fungerar.','2017-02-06 23:12:53',NULL),
	(487,38,106,1,4.00,'Spelar in voiceover-test för klimatnyttan. 3:40sek','2017-02-07 18:31:05',NULL),
	(488,38,106,1,1.00,'Möte hos Norra, hjälper Ulrica med text som var överlagrat så det blev gråtonat.','2017-02-06 18:37:43',NULL),
	(489,11,110,1,2.00,'Fixar nav dots for gallery på products. fixer h1/h2 på splash. lägger upp på demo.morningdew','2017-02-08 11:19:37',NULL),
	(490,11,110,1,3.00,'Lista Application Tags (keywords), separate side för application by keyword.','2017-02-08 19:41:10',NULL),
	(491,11,110,1,8.00,'Gör om bilden (volume) från svg till html på Home, lägger in S1... på Home sidan. Fixar css på gallery (products) så det ser bra ut i mobil. Fixar lista på relevant applications.','2017-02-09 14:55:01',NULL),
	(492,39,109,1,3.00,'Designer led, skickar till Susanne..','2017-02-10 12:02:58',NULL),
	(493,11,110,1,8.00,'Fixar Priority column på Application - Tags  i admin. Visa Show as related i admin-list (Applications).','2017-02-10 22:51:30',NULL),
	(494,11,110,1,1.00,'Lägger Product Gallery live, uppdaterar Spinchem wp, plugins och lägger in fält.','2017-02-10 23:24:11',NULL),
	(495,38,106,1,4.00,'Storyboard efter ny dialog, scen 1.1, 1.2, 1.3.','2017-02-13 21:14:38',NULL),
	(496,29,108,1,3.00,'Kretsloppsbild_01-2, flyttar siffrorna så 1 är uppe till vänster.','2017-02-13 21:16:30',NULL),
	(497,11,110,1,0.30,'Fixar volymer på home.','2017-02-13 21:23:55',NULL),
	(498,40,111,1,4.00,'Skapar logoförslag (5 varianter) för Norra Play.','2017-02-14 11:55:42',NULL);

/*!40000 ALTER TABLE `time_stamps` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(64) DEFAULT NULL,
  `lastname` varchar(64) DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  `edate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `firstname`, `lastname`, `cdate`, `edate`)
VALUES
	(1,'Mattias','Holmgren','2015-09-18 08:00:00',NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
