<?php
	$f_user_type = (isset($_REQUEST["f_user_type"])?$_REQUEST["f_user_type"]:"");
?><div class="my-row"><h1>Användare</h1></div>
	
<div class="my-row full-width"><hr /></div>
<br style="clear:both;" />
<div class="my-row">
	<div class="set-width">
		<input class="button create_user_button login-button" type="submit" value="Skapa användare" />
		
	</div>
	<br style="clear:both;" />
</div>
<br style="clear:both;" />

<div class="my-row full-width"><hr /></div>
<br style="clear:both;" />

<div class="my-row">
	<label>Välj anv.typ</label>
		<select class="anv-typ-selector">
			<option value="">Visa alla typer</option>
			<?php 
				require_once(dirname(__FILE__) . "/../config/settings.php");
				require_once(dirname(__FILE__) . "/../db.php");
				$qry = "SELECT * FROM user_types";
				$user_types = array();
				if ($stmt = $db->mysqli()->prepare($qry)) {
					if ($stmt->execute()) {
						$user_types = $db->get_results($stmt);
						
					}
					$stmt->close();
				}
				
				
				foreach ($user_types as $uT) {?>
				<option value="<?php echo $uT["f_id"]; ?>"<?php echo $f_user_type == $uT["f_id"]?" selected=\"selected\"":""?>><?php echo $uT["f_name"]; ?></option>
				<?php }
			?>
		</select>
		<br style="clear:both;" />
		<small class="hidden error">Välj en betaltyp!</small>
</div>

<div class="my-row full-width"><hr /></div>
<br style="clear:both;" />
<?php
	

	$orderBy = (isset($_REQUEST["orderBy"])?$_REQUEST["orderBy"]:"f_creation_date");
	
	$userFields = array (
		array("title" => "ID","field" => "f_id","class" => ""),
		array("title" => "Användarnamn","field" => "f_login_name","class" => ""),
		array("title" => "Användartyp","field" => "f_user_type_name","class" => ""),
		array("title" => "HK-Kontor","field" => "f_region_name","class" => ""),
		array("title" => "SäljarID","field" => "f_sales_id","class" => "f_sales_id_column"),
		array("title" => "Datum skapad","field" => "f_creation_date","class" => "")
	);
	$foundInArr = false;
	foreach ($userFields as $f) {
		if ($orderBy == $f["field"])
			$foundInArr = true;
	}
	if ($foundInArr == false)
		$orderBy = "f_creation_date";
		
	$ascDesc = (isset($_REQUEST["ascDesc"])?$_REQUEST["ascDesc"]:"DESC"); // Default DESC
	if ($ascDesc != "ASC")
		$ascDesc = "DESC";

	
?>
<table class="orders-table users-table">
	<thead>
		<tr>
			<?php foreach($userFields as $f) { ?>
			<th>
			<a href="<?php 
				echo $GLOBALS["HK_ROOT"] . "anvandare/?orderBy=" . $f["field"] . "&ascDesc=" . (($orderBy==$f["field"] && $ascDesc == "ASC")?"DESC":"ASC");
			?>"><?php echo $f["title"];
				if ($orderBy == $f["field"]) { ?>
				<img src="<?php echo $GLOBALS["HK_ROOT"]; ?>assets/gfx/arrow_<?php echo ($ascDesc == "DESC")?"up":"down"; ?>.svg" class="svg" />
			<?php
				}
			?></a>
			</th>
			<?php } ?>
			<th></th>
		</tr>
	</thead>
	<tbody>
	<?php
		require_once(dirname(__FILE__) . "/users-only.php");
	?>
	</tbody>
</table>

<!--
<div class="my-row">	
	<div class="paging paging-users">
		<div>
	<?php
		if ($page > 0) {
	?>
		<span title="1">&laquo;</span>
		<span class="backward" title="<?php echo $page; ?>">&lt;</span>
	<?php
		}
		$queryParams = array();
		$qry = "SELECT count(f_id) as f_count WHERE 1 = 1 ";
		if ($f_user_type != "") {
			$qry.= " AND f_user_type = ? ";
			array_push($queryParams, $f_user_type);
		}
		$qry .= "FROM users;";
		$totalRows = 0;
		if ($stmt = $db->mysqli()->prepare($qry)) {
			if (count($queryParams) > 0)
				$db->bind_params( $stmt, $queryParams );
			if ($stmt->execute()) {
				$results = $db->get_results($stmt);
				foreach ($results as $row) {
					$totalRows = $row["f_count"];
				}
			}
			$stmt->close();
		}
		// nr of pages:
		$nrOfPages = ceil($totalRows / $usersPerPage);
		$start = $page - 3;
		if ($start < 0)
			$start = 0;
		$end = $page + 3;
		if ($end > $nrOfPages)
			$end = $nrOfPages;
		for ($i = $start; $i < $end; $i++) {
			if ($page == $i)
				echo "<span class=\"active\" title=\"" . ($i+1) . "\">" . ($i+1) . "</span>";
			else
				echo "<span title=\"" . ($i+1) . "\">" . ($i+1) . "</span>";
		}
		if (($page+1) < $nrOfPages) {
	?>
			<span class="forward" title="<?php echo $start+4; ?>">&gt;</span>
			<span title="<?php echo $nrOfPages; ?>">&raquo;</span>
		<?php } ?>
		</div>
	</div>
	<input type="hidden" class="totalPages" value="<?php echo $nrOfPages; ?>" />
	<br style="clear:both;"/>
</div>
/-->
<script type="text/javascript">
	$(function() {
		$(".anv-typ-selector").on("change", function() {
			window.location = "?f_user_type=" + $(this).val();
		});
		//paymentType = $(".hk-payment-type-selector-row span").html();
		// find id by name
		//paymentTypeAsNr = $('.hk-payment-type-selector option').filter(function () { return $(this).html() == paymentType; }).val();
	});
</script>