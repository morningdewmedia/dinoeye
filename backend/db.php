<?php

//	require_once('../config/db_conf.php');
/*
	if( file_exists('../config/db_conf.php') ) {
	   require_once('../config/db_conf.php');
		echo $db_host;
	} else {
		echo "Config file not found.";
	}
*/
	require_once('../config/class.config.php');

	ini_set('mysql.allow_persistent', 'Off');

	// required for old crap versions of PHP
	function fetch_array_from_statement($iStateMent) { 
       $data = mysqli_stmt_result_metadata($iStateMent); 
        $count = 1;
        $fieldnames[0] = &$iStateMent; 
        while ($field = mysqli_fetch_field($data)) { 
            $fieldnames[$count] = &$array[$field->name]; //load the fieldnames into an array. 
            $count++; 
        } 
        call_user_func_array(mysqli_stmt_bind_result, $fieldnames); 
        mysqli_stmt_fetch($iStateMent); 
        return $array; 
    } 

	class Db {
		var $mysqli;
		var $db_config;
		
		var $db_name;
		var $db_host;
		var $db_user;
		var $db_pass;
		var $db_md5k;

		public function __construct() {
			$this->db_config = new Config();

			$db_name = $this->db_config->m_db; 	//"dinoeye";
			$db_host = $this->db_config->m_host;	//ini_get( "mysqli.default_host" );
			$db_user = $this->db_config->m_user;	//ini_get( "mysqli.default_user" );
			$db_pass = $this->db_config->m_pass;	//ini_get( "mysqli.default_pw" );
			$db_md5k = "";
		//	if( file_exists(dirname(__FILE__) . '/config/db-conf.php') ) {
		//		require_once(dirname(__FILE__) . '/config/db-conf.php');
		//	}

			$this->db_md5k = $db_md5k;
		//	echo "host: " . $db_host . " - user: " . $db_user . " - pass: " . $db_pass . " - db: " . $db_name . "<br /><br />";

			$this->mysqli = new mysqli( $db_host, $db_user, $db_pass, $db_name );
			
			if ( mysqli_connect_error() ) {
				die('Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error() );
			}
			$this->mysqli->set_charset( "utf8" );
		}
		public function __destruct() {
			$this->mysqli->close();
		}
		
		public function mysqli() {
			return $this->mysqli;
		}
		
		public function md5_key() {
			return $this->db_md5k;
		}
		
		public function bind_params( &$iStatement, $iParams ) {
			array_unshift($iParams,$this->getParamTypesFromValues($iParams));
            call_user_func_array(array($iStatement, 'bind_param'), $this->makeValuesReferenced($iParams));
		}
		
		public function get_results( &$iStatement ) {
			$result = array();
			$md = $iStatement->result_metadata();
			$params = array();
			$row = array();
			while($field = $md->fetch_field()) {
				$params[] = &$row[$field->name];
			}
			if (call_user_func_array(array($iStatement, 'bind_result'), $params) !== FALSE) {
				while ($iStatement->fetch()) { 
					foreach($row as $key => $val) { 
						$c[$key] = $val; 
					} 
					array_push($result,$c); 
				} 
			}
			return $result;
		}
		
		public function getParamTypesFromValues( $val )
		{
			
				$types = '';                        //initial sting with types
				foreach($val as $para) 
				{     
					if(is_int($para)) {
						$types .= 'i';              //integer
					} elseif (is_float($para)) {
						$types .= 'd';              //double
					} elseif (is_string($para)) {
						$types .= 's';              //string
					} else {
						$types .= 'b';              //blob and unknown
					}
				}
				return $types;
		}
		public function makeValuesReferenced( &$arr )
		{
			$refs = array();
			foreach($arr as $key => $value)
				$refs[$key] = &$arr[$key];
			return $refs;
		}
		
	}

?>
