<?php

//	require_once(dirname(__FILE__) . "/../config/settings.php");
//	require_once("../config/db_conf.php");
	require_once("db.php");
	require_once("dinoeye_class.php");
	
	$req = isset($_REQUEST["req"])?$_REQUEST["req"]:"";
	$project_id = isset($_REQUEST["project_id"])?$_REQUEST["project_id"]:"";
	$milestone_id = isset($_REQUEST["milestone_id"])?$_REQUEST["milestone_id"]:"";
	$stamp_hours = isset($_REQUEST["stamp_hours"])?$_REQUEST["stamp_hours"]:"";
	$stamp_name = isset($_REQUEST["stamp_name"])?$_REQUEST["stamp_name"]:"";
	$stamp_date = isset($_REQUEST["stamp_date"])?$_REQUEST["stamp_date"]:"";

	$name = isset($_REQUEST["name"])?$_REQUEST["name"]:"";
	$priceHour = isset($_REQUEST["priceHour"])?$_REQUEST["priceHour"]:"";
	$priceProject = isset($_REQUEST["priceProject"])?$_REQUEST["priceProject"]:"";
	$currency = isset($_REQUEST["currency"])?$_REQUEST["currency"]:"";
	$status = isset($_REQUEST["status"])?$_REQUEST["status"]:"";
	$estimateHours = isset($_REQUEST["estimateHours"])?$_REQUEST["estimateHours"]:"";
	$edate = date("Y-m-d H:i:s");

	//echo $project_id;
	$db = new Db();
	$dinoeye_handler = new Dinoeye();
	
//	echo "req: $req <br />";
	if($req == "save_milestone" ) {

		$qry = "UPDATE milestones SET project_id = ?, milestone_name = ?, estimate_to_client = ?, price_per_hour = ?, price_per_project = ?, currency = ?, status = ?, edate = ?"; 
		$qry .= " WHERE id = ?";
	//	$new_row_id = null;
	//	$totalRows = 0;

		if ($stmt = $db->mysqli()->prepare($qry)) {
			$returnJSON = "{\"status\":\"hmmm\",\"result\":\"No problem\"}";

			$db->bind_params($stmt, array( $project_id, $name, $estimateHours, $priceHour, $priceProject, $currency, $status, $edate, $milestone_id ));

			if ($stmt->execute()) {
				$new_row_id = $stmt->insert_id;
			//	printf ("New Record has id %d.\n", $new_row_id);
				$returnJSON = "{\"status\":\"ok\",\"result\":\"No problem\"}";
			} else {
			//	printf ("Error %d.\n", $mysqli->error);			
				$returnJSON = "{\"status\":\"error\",\"result\":\"Could not update row!" . $db->mysqli()->error . "\"}";	
			}
			$stmt->close();
		} else {
			$returnJSON = "{\"status\":\"error\",\"result\":\"" . $db->mysqli()->error . "\"}";
		}
		
		echo $returnJSON;

	}
	else if($req == "open_milestone_edit" ) {
	//	$milestone_id = 1;
		$milestone = $dinoeye_handler->getMilestoneByID($milestone_id);
		$project = $dinoeye_handler->getProjectByID($milestone["project_id"]);
		$project_id = $milestone["project_id"];
		$project_name = $project["name"];

		$milestone_statuses = $dinoeye_handler->getMilestoneStatusTypes();
		$currencies = $dinoeye_handler->getCurrencyTypes();

		$ret = '<h2>Edit Milestone</h2>';
		$ret .= '<form class="m_form" method="post" action="">';
/*		$ret .= '<input name="form_project_id" type="hidden">'; */
		$ret .= '<input id="form_milestone_id" name="form_milestone_id" type="hidden" value="' . $milestone_id . '">';
		$ret .= '<input id="m_project_id" name="m_project_id" type="hidden" value="' . $project_id . '">';
	//	$ret .= '<input id="m_user_id" name="m_user_id" type="hidden" value="' . $user_id . '">';
		$ret .= '<div class="block"><label>Milestone:</label><input type="text" name="milestone_name" id="m_name" class="m_name" placeholder="Milestone name" value="' . $milestone["milestone_name"] . '" /></div>';
		$ret .= '<div class="block"><label>Project Name:</label><input type="text" name="project_name" id="m_project_name" class="m_project_name" placeholder="Project name" value="' . $project_name . '"/></div>';
		$ret .= '<div class="block"><label>Estimate hours:</label> <input type="text" name="estimate_hours" id="m_estimate_hours" class="m_estimate_hours" placeholder="estimate hours" value="' . $milestone["estimate_to_client"] . '" /></div>';
		$ret .= '<div class="block"><label>Price / hour:</label> <input type="text" name="price_hour" id="m_price_hour" class="m_price_hour" placeholder="price per hour" value="' . $milestone["price_per_hour"] . '" /></div>';
		$ret .= '<div class="block"><label>Price / project:</label> <input type="text" name="price_project" id="m_price_project" class="m_price_project" placeholder="price per project" value="' . $milestone["price_per_project"] . '" /></div>';
	//	$ret .= '<div class="block"><label>Currency:</label> <input type="text" name="currency" class="m_currency" placeholder="currency" value="' . $milestone["currency"] . '" /></div>';
		$ret .= '<select id="m_currency" class="m_currency" name="m_currency">';

		foreach($currencies as $itm) {
			if($milestone["currency"] == $itm["id"]) {
				$ret .= '<option value="' . $itm["id"] .'" selected >' . $itm["name"] . '</option>';
			} else {
				$ret .= '<option value="' . $itm["id"] .'">' . $itm["name"] . '</option>';
			}
		}

		$ret .= '</select>';


		$ret .= '<div class="block"><label>Status:</label>'; // <input type="text" name="status" class="m_status" placeholder="status" value="' . $milestone["status"] . '" /></div>';
		$ret .= '<select id="m_status" class="m_status" name="milestone_status" >';

		foreach($milestone_statuses as $itm) {
			$s = $itm["status"];
			if($milestone["status"] == $itm["id"]) {
				$ret .= '<option value="' . $itm["id"] .'" selected >' . $s . '</option>';
			} else {
				$ret .= '<option value="' . $itm["id"] .'">' . $s . '</option>';
			}
		}

		$ret .= '</select>';
		$ret .= '<br /><br />';
		$ret .= '<div class="block">Cdate: 2016-01-25</div>';
		$ret .= '<div class="block">Edate: 2016-01-25</div>';
		$ret .= '<input id="submit_milestone" type="submit" value="submit" >';
		$ret .= '</form>';

	//	print_r($milestone);

		echo $ret;
	}
	else if($req == "get_projects" || $req == "get_finished_projects") {
		$queryParams = array();
	//	$sel = "projects.id as p_id, projects.name, milestones.id as m_id, milestones.milestone_name";
	//	$qry = "SELECT " . $sel . " FROM projects inner join milestones on projects.id = milestones.project_id limit 0,1000";
		$sel = "projects.id, projects.name, projects.cdate, projects.edate";

		if($req == "get_finished_projects") {
			$status = "WHERE status = '3'";
			$typ = "";
		} else {
			$status = "WHERE status = '2'";
		}

		$qry = "SELECT " . $sel . " FROM projects " . $status . " ORDER BY cdate DESC limit 0,1000";

		//$qry = " INNER JOIN clients where "

		// select * from time_stamps inner join clients on clients.id = "1" where project_id = "1"

	//	array_push($queryParams, $project_id);


		$totalRows = 0;

		if ($stmt = $db->mysqli()->prepare($qry)) {
			if (count($queryParams) > 0)
				$db->bind_params( $stmt, $queryParams );

			if ($stmt->execute()) {

				$results = $db->get_results($stmt);
				/*
				foreach ($results as $row) {
					$totalRows = $row["f_count"];
				}
				*/

			}
			$stmt->close();
		}

		//	print_r($results);
		echo "<ul>";

		foreach($results as $row) {
			$proj_id = $row["id"];
			$proj_name = $row["name"];
			$milestone_id = $row["id"];
			$proj_cdate = $row["cdate"];
			$proj_edate = $row["edate"];

			// fetch milestones
			$sel = "id, milestone_name, estimate_to_client, status";
			$qry = "SELECT " . $sel . " FROM milestones where project_id = " . $proj_id . " ORDER BY cdate DESC limit 0,1000";
		//	echo $qry;
			$totalRows = 0;

			if ($stmt = $db->mysqli()->prepare($qry)) {
				if (count($queryParams) > 0)
					$db->bind_params( $stmt, $queryParams );

				if ($stmt->execute()) {
					$milestones_result = $db->get_results($stmt);
				}
				$stmt->close();
			}

		//	$project_name_format = $proj_name . " - " . $row["id"];
			$project_name_format = $proj_name;


			// sum all hours from milestones

			$proj_timestamps = $dinoeye_handler->getTimeStampsFromProject($proj_id);
			$proj_time_arr = $dinoeye_handler->sumAllStamps($proj_timestamps);

			$proj_total_time = $proj_time_arr["hours"] . "." . $proj_time_arr["minutes"];

			echo "
				<li id='list_item_proj-" . $proj_id ."'>
                    <div class='proj_btn'><div class='open_status'>+</div> " . $project_name_format . ' - ' . $proj_total_time . '</div>
                    <ul class="milestones">
                    ';

                    foreach($milestones_result as $milestone_row) {

                    	$milestone_id = $milestone_row["id"];
                    	$milestone_name = $milestone_row["milestone_name"]; // . " " . $milestone_id;
                    	$estimate_to_client = $milestone_row["estimate_to_client"];
                    	$status = $milestone_row["status"];

                    	// sum all the timestamps from this milestone
                    	$timestamps = $dinoeye_handler->getTimeStampsFromMilestone($milestone_id);

                    	$time_arr = $dinoeye_handler->sumAllStamps($timestamps);
                    	
                    //	print_r($time_arr);
                    	$time_spent = $time_arr["hours"] . "." . $time_arr["minutes"] . " / " . $estimate_to_client . " h";

                    	$percent_of_total = ceil(($time_arr["hours"] . "." . $time_arr["minutes"]) / $estimate_to_client * 100);
                    	
                    	if($percent_of_total<51) $color_class = "blue_bar";
                    	else if ($percent_of_total>50 && $percent_of_total<70) $color_class = "green_bar";
                    	else if ($percent_of_total>69 && $percent_of_total<100) $color_class = "yellow_bar";
                    	else if ($percent_of_total>99 && $percent_of_total<150) $color_class = "red_bar";
                    	else if ($percent_of_total>149) $color_class = "red_bar";

	                    echo "
	                      <li> 
	                        <a class='milestone' id='proj-" . $proj_id . "_milestone-" . $milestone_id . "' title='Milestone " . $milestone_id . "' >" . $milestone_name . "</a>
	                        <div class='ml_hours'>
	                        	" . $time_spent . '
	                        </div>
	                       '; // <img src="images/milestone_status_02.svg" class="svg" alt="Status" title="Status" />
	                       
	                       switch($status) {
	                       	case 1:
	                       		milestone_status_01();
	                       		break;
	                       	case 2:
	                       		milestone_status_02();
	                       		break;
	                       	case 3:
	                       		milestone_status_03();
	                       		break;
	                       	case 4:
	                       		milestone_status_04();
	                       		break;
	                       	case 5:
	                       		milestone_status_05();
	                       		break;
	                       	default:
	                       		break;
	                       	}

	                      echo '
	                      	<div class="status_bar ' . $color_class . '">
	                      		<progress value="' . $percent_of_total . '" max="100"></progress>
	                      		' . $percent_of_total . ' % 
	                      	</div>
	                      </li>
	                    ';
	                }

             echo "
                    </ul>
                </li>
			";
		}

		echo "</ul>";

	} // endif get_milestones
	else if($req == "get_timestamps") {
		$queryParams = array();
		$sel = "time_stamps.id, time_stamps.time_spent, time_stamps.task_name, time_stamps.cdate as time_stamp_date, users.id as user_id, users.firstname, users.lastname";
		$qry = "SELECT " . $sel . " FROM time_stamps inner join users on users.id = time_stamps.user_id where milestone_id = ?";
		$qry .= " ORDER By time_stamps.cdate DESC";
		//$qry = " INNER JOIN clients where "

		// select * from time_stamps inner join clients on clients.id = "1" where project_id = "1"

		array_push($queryParams, $milestone_id);

		$totalRows = 0;

		if ($stmt = $db->mysqli()->prepare($qry)) {
			if (count($queryParams) > 0)
				$db->bind_params( $stmt, $queryParams );

			if ($stmt->execute()) {

				$results = $db->get_results($stmt);
				/*
				foreach ($results as $row) {
					$totalRows = $row["f_count"];
				}
				*/

			}
			$stmt->close();
		}

		//	print_r($results);
		echo '<h4>TimeStamps</h4>
            <div class="milestone_name">Milestone name...</div>
            <hr />
            <div class="add_new_timestamp">
                <h6>Add new timestamp</h6>
                <form id="new_timestamp_form" action="backend/backend.php">
                  <input id="stamp_name" type="text" value="" placeholder="task..."/>
                  <input id="stamp_hours" type="text" value="" placeholder="hours..." />
                  <input id="datepicker" type="text" placeholder="date..." />
                  <input id="stamp_form_project_id" type="hidden" value="" />
                  <input id="stamp_form_milestone_id" type="hidden" value="" />
                  <input id="submit_stamp" type="submit" value="submit" />
                </form>
            </div>
            <div id="timestamp_content">
        ';

		foreach($results as $row) {
			echo '
				<div id="content">
					<div class="timestamp" data-timestamp-id="' . $row["id"] . '" >
						<div class="timestamp_bubble" >
							' . $row["time_spent"] . ' h - ' . $row["task_name"] . '
						</div>
						<div class="user_reg">
		                	' . $row["firstname"] . ' ' . $row["lastname"] . ' - 
		                	' . $row["time_stamp_date"] . '
		                </div>
		            </div>
				</div>
			';
		}

		echo '
            </div>
           ';

	} // endif get_milestones
	else if($req == "create_timestamp") {
		if (strpos($stamp_date,' ') !== false) {			// we probably have hours:min:sec in this date
			// do nothing
		}
		else if($stamp_date == "") {						// inget datum skickades, skapa dagens datum
			$stamp_date = date("Y-m-d H:i:s");
		} else {
			$stamp_date .= " " . date("H:i:s");				// lägg på tid för idag.
		}

		$queryParams = array();

		$qry = "INSERT INTO time_stamps (project_id, milestone_id, user_id, time_spent, task_name, cdate) VALUES (?,?,?,?,?,?)";

		$new_row_id = null;
		$totalRows = 0;

		if ($stmt = $db->mysqli()->prepare($qry)) {
			$db->bind_params($stmt, array( $project_id, $milestone_id, "1", $stamp_hours, $stamp_name, $stamp_date ));

			if ($stmt->execute()) {
				$new_row_id = $stmt->insert_id;
			//	printf ("New Record has id %d.\n", $new_row_id);
				$returnJSON = "{\"status\":\"ok\",\"result\":\"No problem\"}";
			} else {
			//	printf ("Error %d.\n", $mysqli->error);			
				$returnJSON = "{\"status\":\"error\",\"result\":\"Could not insert order!" . $db->mysqli()->error . "\"}";	
			}
			$stmt->close();
		}
		
		echo $returnJSON;

	} // endif get_milestones
	else if($req == "get_status") {
		echo '<div class="btn" data-set_status="not_started" >';
		milestone_status_01();
		echo "<div>Not started</div></div>";
		echo '<div class="btn" data-set_status="in_progress" >';
		milestone_status_02();
		echo "<div>In progress</div></div>";
		echo "<div>";
		milestone_status_03();
		echo "<div>Ready to invoice</div></div>";
		echo "<div>";
		milestone_status_04();
		echo "<div>Invoice sent</div></div>";
		echo "<div>";
		milestone_status_05();
		echo "<div>Payment received</div></div>";
	}
	else if($req == "get_clients-db") {
		$returnJSON = "{\"status\":\"hmmm\",\"result\":\"No problem\"}";
		$queryParams = array();
	//	$sel = "projects.id as p_id, projects.name, milestones.id as m_id, milestones.milestone_name";
	//	$qry = "SELECT " . $sel . " FROM projects inner join milestones on projects.id = milestones.project_id limit 0,1000";
		$sel = "clients.id, clients.company, clients.contact_firstname, clients.contact_lastname, clients.email, clients.email_invoice, clients.currency, clients.cdate, clients.edate";
/*
		if($req == "get_finished_projects") {
			$status = "WHERE status = '3'";
			$typ = "";
		} else {
			$status = "WHERE status = '2'";
		}
*/
		$qry = "SELECT " . $sel . " FROM clients " . $status . " ORDER BY cdate DESC limit 0,1000";

		//$qry = " INNER JOIN clients where "

		// select * from time_stamps inner join clients on clients.id = "1" where project_id = "1"

	//	array_push($queryParams, $project_id);


		$totalRows = 0;

		if ($stmt = $db->mysqli()->prepare($qry)) {
			if (count($queryParams) > 0)
				$db->bind_params( $stmt, $queryParams );

			if ($stmt->execute()) {
				$results["result"] = $db->get_results($stmt);
				
				$returnJSON = "{\"status\":\"ok\",\"result\":\"No problem\"}";

				/*
				foreach($results as $row) {
					$proj_id = $row["id"];
					$proj_name = $row["name"];
					$milestone_id = $row["id"];
					$proj_cdate = $row["cdate"];
					$proj_edate = $row["edate"];

				}
				*/
				/*
				$emparray = array();
				while($row = mysqli_fetch_assoc($results))
			    {
			        $emparray[] = $row;
			    }
			        echo json_encode($emparray);
			    */
			    $results["status"] = "ok";

			    echo json_encode($results);
			   

			} else {
			//	printf ("Error %d.\n", $mysqli->error);			
				$returnJSON = "{\"status\":\"error\",\"result\":\"Could not update row!" . $db->mysqli()->error . "\"}";	
			}
			$stmt->close();
		} else {
			$returnJSON = "{\"status\":\"error\",\"result\":\"" . $db->mysqli()->error . "\"}";
		}

	//	echo $returnJSON;

	}

	function milestone_status_01() {
		echo '
        <svg class="small_icon" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 viewBox="0 0 32 32" enable-background="new 0 0 32 32" xml:space="preserve">
			<g id="Layer_1">
				<circle fill="none" stroke="#000000" stroke-width="4" stroke-miterlimit="10" cx="16" cy="16.6" r="13.4"/>
			</g>
			<g id="Layer_2">
			</g>
		</svg>
        ';
	}

	function milestone_status_02() {
		echo '
           <svg class="small_icon" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 viewBox="0 0 32 32" enable-background="new 0 0 32 32" xml:space="preserve">
			<g id="Layer_1">
				<circle fill="none" stroke="#000000" stroke-width="4" stroke-miterlimit="10" cx="16" cy="16.6" r="13.4"/>
			</g>
			<g id="Layer_2">
				<path d="M16,8c-5,0-9,4-9,9c0,5,4,9,9,9V8z"/>
			</g>
			</svg>
        ';
	}

	function milestone_status_03() {
		echo '
          <svg class="small_icon" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 viewBox="0 0 32 32" enable-background="new 0 0 32 32" xml:space="preserve">
			<g id="Layer_1">
				<circle fill="none" stroke="#000000" stroke-width="4" stroke-miterlimit="10" cx="16" cy="16.6" r="13.4"/>
			</g>
			<g id="Layer_2">
				<circle cx="16" cy="17" r="9"/>
			</g>
			</svg>
        ';
	}

	function milestone_status_04() {
		echo '
           <svg class="small_icon" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 viewBox="0 0 32 32" enable-background="new 0 0 32 32" xml:space="preserve">
			<g id="Layer_1">
				<circle fill="none" stroke="#000000" stroke-width="4" stroke-miterlimit="10" cx="16" cy="16.6" r="13.4"/>
			</g>
			<g id="Layer_2">
				<polygon points="23,22 9,22 9,15 16,19 23,15 	"/>
				<polygon points="9,12 16,16 23,12 23,11 9,11 	"/>
			</g>
			</svg>
        ';
	}

	function milestone_status_05() {
		echo '
           <svg class="small_icon" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 viewBox="0 0 32 32" enable-background="new 0 0 32 32" xml:space="preserve">
			<g id="Layer_1">
				<circle fill="none" stroke="#000000" stroke-width="4" stroke-miterlimit="10" cx="16" cy="16.6" r="13.4"/>
			</g>
			<g id="Layer_2">
				<g>
					<path d="M17,24.6c-1,0-2-0.2-2.9-0.6c-0.9-0.4-1.7-1-2.3-1.7c-0.8-0.9-1.4-1.9-1.8-3H8.5v-2h1.2c0-0.2,0-0.3,0-0.5s0-0.3,0-0.5
						H8.5v-2H10c0.4-1.1,0.9-2.2,1.8-3c0.7-0.7,1.4-1.3,2.3-1.7C15,9.2,16,9,17,9c1.1,0,2.2,0.3,3.2,0.8l-1.1,2.2
						c-0.7-0.3-1.4-0.5-2.1-0.5c-1.9,0-3.5,1.2-4.3,2.9h5.8v2h-6.3c0,0.1,0,0.3,0,0.5s0,0.3,0,0.5h6.3v2h-5.8c0.8,1.7,2.5,2.9,4.3,2.9
						c0.8,0,1.5-0.2,2.1-0.5l1.2,2.2C19.3,24.3,18.2,24.6,17,24.6z"/>
				</g>
			</g>
			</svg>
        ';
	}


?>