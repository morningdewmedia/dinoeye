<?php
	require_once("db.php");

	class Dinoeye {
		var $db;

		public function __construct() {
			$this->db = new Db();
		}

		public function getMilestoneByID($i_id) {
			$sel = "id, project_id, milestone_name, estimate_to_client, price_per_hour, price_per_project, currency, status, cdate, edate";
			$qry = "SELECT " . $sel . " FROM milestones where id = ? limit 1";

			$totalRows = 0;
			$queryParams = array($i_id);
			$result = "";
			
			if ($stmt = $this->db->mysqli()->prepare($qry)) {
				if (count($queryParams) > 0)
					$this->db->bind_params( $stmt, $queryParams );

				if ($stmt->execute()) {
					$result = $this->db->get_results($stmt);
				}
				$stmt->close();
			}
			
			
			return $result[0];
		}

		public function getProjectByID($i_id) {
			$sel = "id, name, status, cdate, edate";
			$qry = "SELECT " . $sel . " FROM projects where id = ? limit 1";

			$totalRows = 0;
			$queryParams = array($i_id);
			$result = "";
			
			if ($stmt = $this->db->mysqli()->prepare($qry)) {
				if (count($queryParams) > 0)
					$this->db->bind_params( $stmt, $queryParams );

				if ($stmt->execute()) {
					$result = $this->db->get_results($stmt);
				}
				$stmt->close();
			}
			
			
			return $result[0];
		}

		public function getTimeStampsFromMilestone($i_id) {
			$sel = "id, milestone_id, time_spent, cdate";
			$qry = "SELECT " . $sel . " FROM time_stamps where milestone_id = ? limit 0,1000";
		//	echo $qry;
			$totalRows = 0;
			$timestamps_result = array();

			$queryParams = array($i_id);

			if ($stmt = $this->db->mysqli()->prepare($qry)) {
				if (count($queryParams) > 0)
					$this->db->bind_params( $stmt, $queryParams );

				if ($stmt->execute()) {
					$timestamps_result = $this->db->get_results($stmt);
				}
				$stmt->close();
			}
			return $timestamps_result;
		}

		public function getTimeStampsFromProject($i_id) {
		//	echo "project_id" . $i_id;
			$sel = "id, milestone_id, time_spent";
			$qry = "SELECT " . $sel . " FROM time_stamps where project_id = ? limit 0,1000";
		
			$totalRows = 0;
			$timestamps_result = array();

			$queryParams = array($i_id);

			if ($stmt = $this->db->mysqli()->prepare($qry)) {
				if (count($queryParams) > 0)
					$this->db->bind_params( $stmt, $queryParams );

				if ($stmt->execute()) {
					$timestamps_result = $this->db->get_results($stmt);
				}
				$stmt->close();
			}

		//	print_r($timestamps_result);

			return $timestamps_result;
		}

		public function getMilestoneStatusTypes() {
			$qry = "SELECT * FROM milestone_statuses limit 0,100";

			if($stmt = $this->db->mysqli()->prepare($qry)) {
				if ($stmt->execute()) {
					$result = $this->db->get_results($stmt);
				}
				$stmt->close();
			}
//			var_dump($result);

			return $result;
		}

		public function getCurrencyTypes() {
			$qry = "SELECT * FROM currency limit 0,100";

			if($stmt = $this->db->mysqli()->prepare($qry)) {
				if ($stmt->execute()) {
					$result = $this->db->get_results($stmt);
				}
				$stmt->close();
			}
//			var_dump($result);

			return $result;
		}

		public function sumAllStamps($i_stamps) {
			$hours = 0;
			$minutes = 0;
			$time_arr = array();
			$time = array("hours"=>0, "minutes"=>0);

			if($i_stamps!=null) {
				foreach($i_stamps as $stamp) {
					$time_arr = explode(".", $stamp["time_spent"]);
					$time["hours"] += $time_arr[0];
				//	echo $time["hours"] . "<br />";
					$minutes += $time_arr[1];
				}
//				echo "hours:" . $time["hours"] . "<br />";
				$time["hours"] += floor($minutes / 60);

				$time["minutes"] = ($minutes % 60);
			}

			return $time;
		}

	}

?>