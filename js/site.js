$(function () {

	var clients;	// init clients
	var filters = null;

/*
	$.getJSON( "js/products.json", function( data ) {
		// Get data about our products from products.json.
		// Call a function that will turn that data into HTML.
		generateAllClientsHTML(data);

		// Manually trigger a hashchange to start the app.
		$(window).trigger('hashchange');
	});

	*/
	var posting = $.post( "backend/backend.php", { req: "get_clients-db" }, 'json' );

	posting.done(function(data) {
		var response = jQuery.parseJSON(data);

	//	console.log(response.result);
	//	console.log(response.status);

		generateAllClientsHTML2(response);
		$(window).trigger('hashchange');
  	});
	 
	posting.fail(function( jqXHR, textStatus ) {
		// $( "#timestamp_content" ).empty().append( data );
	  alert( "Request failed: " + textStatus );
	});



	$(window).on('hashchange', function(){
		// On every hash change the render function is called with the new hash.
		// This is how the navigation of our app happens.
		render(decodeURI(window.location.hash));
	});

	// Single client page buttons

	var singleClientPage = $('.single-client');

	singleClientPage.on('click', function (e) {

		if (singleClientPage.hasClass('visible')) {

			var clicked = $(e.target);

			// If the close button or the background are clicked go to the previous page.
			if (clicked.hasClass('close') || clicked.hasClass('overlay')) {
				// Change the url hash with the last used filters.
				createQueryHash(filters);
			}

		}

	});

	function createQueryHash(filters){

		// Here we check if filters isn't empty.
		if(!$.isEmptyObject(filters)){
			// Stringify the object via JSON.stringify and write it after the '#filter' keyword.
			window.location.hash = '#filter/' + JSON.stringify(filters);
		}
		else{
			// If it's empty change the hash to '#' (the homepage).
			window.location.hash = '#';
		}

	}


/*
  // Grab the template script
  var theTemplateScript = $("#address-template").html();

  // Compile the template
  var theTemplate = Handlebars.compile(theTemplateScript);

  // Define our data object
  var context={
    "city": "London",
    "street": "Baker Street",
    "number": "221B"
  };

  var context = {
    people: [ 
      { firstName: 'Homer', lastName: 'Simpson' },
      { firstName: 'Peter', lastName: 'Griffin' },
      { firstName: 'Eric', lastName: 'Cartman' },
      { firstName: 'Kenny', lastName: 'McCormick' },
      { firstName: 'Bart', lastName: 'Simpson' }
    ]
  };

  // Pass our data to the template
  var theCompiledHtml = theTemplate(context);

  // Add the compiled html to the page
  $('.content-placeholder').html(theCompiledHtml);
*/

});

/* Render the page after navigating, fetch the url from the browser-hash */

function render(url) {
	console.log("ooh");
	// Get the keyword from the url.
	var temp = url.split('/')[0];

	// Hide whatever page is currently shown.
	$('.my_app .page').removeClass('visible');
	console.log("invisible - " + temp);

	var map = {

		// The Homepage.
		'': function() {

			// Clear the filters object, uncheck all checkboxes, show all the products
			filters = {};
		//	checkboxes.prop('checked',false);

		//	renderProductsPage(products);
			console.log("Page: Init-start.");
			renderAllClientsPage(clients);
		},

		// Single Client page.
		'#client': function() {

			// Get the index of which product we want to show and call the appropriate function.
			var index = url.split('#client/')[1].trim();
			console.log("Page: Single Client.");
			renderSingleClientPage(index, clients);
		},

		// Page with filtered products
		'#filter': function() {

			// Grab the string after the '#filter/' keyword. Call the filtering function.
			url = url.split('#filter/')[1].trim();

			// Try and parse the filters object from the query string.
			try {
				filters = JSON.parse(url);
			}
			// If it isn't a valid json, go back to homepage ( the rest of the code won't be executed ).
			catch(err) {
				window.location.hash = '#';
			}

		//	renderFilterResults(filters, products);
		}

	};

	// Execute the needed function depending on the url keyword (stored in temp).
	if(map[temp]){
		map[temp]();
	}
	// If the keyword isn't listed in the above - render the error page.
	else {
		renderErrorPage();
	}

}


function generateAllClientsHTML2(data){
	clientData = data["result"];
	console.log(clientData);
	//console.log(data);
    var list = $('.all-clients .clients-list');

    var theTemplateScript = $("#clients-template").html();
    //Compile the template​
    var theTemplate = Handlebars.compile(theTemplateScript);
    list.append (theTemplate(data));

    clients = clientData;

    // Each products has a data-index attribute.
    // On click change the url hash to open up a preview for this product only.
    // Remember: every hashchange triggers the render function.
    list.find('li').on('click', function (e) {
      e.preventDefault();
      console.log("clicked...");

      var clientIndex = $(this).data('index');

      window.location.hash = 'client/' + clientIndex;
    });

}



function generateAllClientsHTML(data){
	console.log(data);
	//console.log(data);
    var list = $('.all-clients .clients-list');

    var theTemplateScript = $("#clients-template").html();
    //Compile the template​
    var theTemplate = Handlebars.compile(theTemplateScript);
    list.append (theTemplate(data));

    clients = data;

    // Each products has a data-index attribute.
    // On click change the url hash to open up a preview for this product only.
    // Remember: every hashchange triggers the render function.
    list.find('li').on('click', function (e) {
      e.preventDefault();
      console.log("clicked...");

      var clientIndex = $(this).data('index');

      window.location.hash = 'client/' + clientIndex;
    });

}


function renderAllClientsPage(data){

    var page = $('.all-clients'),
      allProducts = $('.all-clients .clients-list > li');

    // Hide all the products in the products list.
    allProducts.addClass('hidden');

    // Iterate over all of the products.
    // If their ID is somewhere in the data object remove the hidden class to reveal them.
    allProducts.each(function () {

      var that = $(this);

      data.forEach(function (item) {
        if(that.data('index') == item.id){
          that.removeClass('hidden');
        }
      });
    });

    // Show the page itself.
    // (the render function hides all pages so we need to show the one we want).
    page.addClass('visible');

}


function renderSingleClientPage(index, data){

    var page = $('.single-client'),
      container = $('.preview-large');

    // Find the wanted product by iterating the data object and searching for the chosen index.
    if(data.length){
      data.forEach(function (item) {
        if(item.id == index){
          // Populate '.preview-large' with the chosen product's data.
          page.find('h2').text(item.name);
          page.find('img').attr('src', item.image.large);
          page.find('p').text(item.description);
        }
      });
    }

    // Show the page.
    page.addClass('visible');

  }


function renderErrorPage(){
    var page = $('.error');
    page.addClass('visible');
}




function attach_project_functions() {
	$(".projects .proj_btn").off().on("click", function(e) {
		// Ladda timestamps till div
		console.log("clicked project.");

		var i = $(this).parent().attr("id");
		var proj = i.substr(15);			// remove "list_item_proj-" from string

		$("#stamp_form_project_id").val(proj);	// update timestamp form with project id
	
		var milestones = $(this).next();
		if ( milestones.is( ":hidden" ) ) {
			$(this).next().show( "fast" );
			$(this).children(1).html("-");
		} else {
			$(this).next().slideUp();
			$(this).children(1).html("+");
		}
	/*	
		var posting = $.post( "backend/backend.php", { req: "get_milestones", project_id: proj } )
	 
	  	// Put the results in a div
	  	posting.done(function(data) {
	    	$( "#timestamp_content" ).empty().append( data );
	  	});
		 
		posting.fail(function( jqXHR, textStatus ) {
			// $( "#timestamp_content" ).empty().append( data );
		  alert( "Request failed: " + textStatus );
		});
*/
		e.preventDefault();
	});

	/////////////////////////////////
	//
	//		New TimeStamp from form data


	$("#new_timestamp_form").off().submit(function( event ) {
  		event.preventDefault();

  		var stampName = $(this).children("#stamp_name").val();
  		var stampHours = $(this).children("#stamp_hours").val();
  		var projectId = $("#stamp_form_project_id").val();
  		var milestoneId = $("#stamp_form_milestone_id").val();
  		var stampDate = $(this).children("#datepicker").val();

  		if(stampName=="" || stampHours=="") {
  			console.log("Empty form...");
  			return;
  		}
  		
  		var posting = $.post( "backend/backend.php", { req: "create_timestamp", project_id: projectId, milestone_id: milestoneId, stamp_name: stampName, stamp_hours: stampHours, stamp_date: stampDate }, 'json' );

		posting.done(function(data) {
			var response = jQuery.parseJSON(data);

			console.log(response.result);
			console.log(response.status);

			// empty the form
			$("#stamp_name").val("");
			$("#stamp_hours").val("");

			// reload timestamps
			UpdateTimeStampsDiv(projectId, milestoneId);

	    	//$( "#timestamp_content" ).empty().append( data );
	  	});
		 
		posting.fail(function( jqXHR, textStatus ) {
			// $( "#timestamp_content" ).empty().append( data );
		  alert( "Request failed: " + textStatus );
		});

	});

	////////////////////////////////////////

}

function UpdateTimeStampsDiv() {
	var projectId = arguments[0];
	var milestoneId = arguments[1];
	
	console.log(projectId);
	console.log(milestoneId);

	var posting = $.post( "backend/backend.php", { req: "get_timestamps", project_id: projectId, milestone_id: milestoneId } )
	 
  	// Put the results in a div
  	posting.done(function(data) {
    	$( "#timestamp_content" ).empty().append( data );
    	populate_projects();
  	});
	 
	posting.fail(function( jqXHR, textStatus ) {
		// $( "#timestamp_content" ).empty().append( data );
	  alert( "Request failed: " + textStatus );
	});
}

function attach_milestone_functions() {
	$(".milestones a").off().on("click", function(e) {
		// Ladda timestamps till div
		console.log("clicked milestone.");

		var i = e.target.id;
		var arr = i.split("_");
		var proj = arr[0].substr(5);			// remove "proj-" from string
		var mile_stone = arr[1].substr(10);		// remove "milestone-" from string
		var milestone_name = $(this).html();
		
	//	alert(proj);
	//	alert(mile_stone);
		
		var posting = $.post( "backend/backend.php", { req: "get_timestamps", project_id: proj, milestone_id: mile_stone } )
	 
	  	// Put the results in a div
	  	posting.done(function(data) {
	    /*	$( "#timestamp_content" ).empty().append( data ); */
	    	$( ".timestamps_bg .my_margin" ).empty().append( data );
	    	$(".milestone_name").html(milestone_name);
	    	$("#datepicker").datepicker({dateFormat: "yy-mm-dd"});
	    	$("#stamp_form_project_id").val(proj);	// update timestamp form with project id
			$("#stamp_form_milestone_id").val(mile_stone);	// update timestamp form with milestone_id

	    	attach_project_functions();
	  	});
		 
		posting.fail(function( jqXHR, textStatus ) {
			// $( "#timestamp_content" ).empty().append( data );
		  alert( "Request failed: " + textStatus );
		});

		e.preventDefault();

	});

	$(".small_icon").off().on("click", function(e) {
		console.log("Open Milestone Edit!");

		/* .... */
	//	var projectId = 1;
	//	var milestoneId = 1;
	//	console.log($(this).parent().parent().parent().attr("id"));
	//	console.log($(this).parent().find("a").attr("id"));
		var proj_milestone = $(this).parent().find("a").attr("id");
		var pm = extractProjectAndMilestoneFromString(proj_milestone);
		projectId = pm["project"];
		milestoneId = pm["milestone"];

		console.log(projectId);
		console.log(milestoneId);

		var posting = $.post( "backend/backend.php", { req: "open_milestone_edit", project_id: projectId, milestone_id: milestoneId } )
		 
	  	// Put the results in a div
	  	posting.done(function(data) {
	    	// $( "#timestamp_content" ).empty().append( data );
	    	$( ".timestamps_bg .my_margin" ).empty().append( data );
	    	// attach save edit_milestone_data

	    	attach_save_milestone_function();
	    	// 

	  	});

	  	e.preventDefault();
	  	return false;

	});
}

function attach_save_milestone_function() {
	console.log("attach Saving milestone!");

	$("#submit_milestone").off().on("click", function( event ) {
  		event.preventDefault();

  		var mId = $("#form_milestone_id").val(); 
  		var mProjectId = $("#m_project_id").val(); 
  		var mStatus = $('#m_status').val();
  		var mName = $('#m_name').val();
  		var mEstimateHours = $('#m_estimate_hours').val();
  		var mPriceHour = $('#m_price_hour').val();
  		var mPriceProject = $('#m_price_project').val();
  		var mCurrency = $('#m_currency').val();
  		
  		
  		console.log("Milestone ID:" + mId);
  		console.log("Project ID:" + mProjectId);
  		console.log("Status:" + mStatus);
  		console.log("Name:" + mName);
  		console.log("Estimate Hours:" + mEstimateHours);
  		console.log("Price / Hour:" + mPriceHour);
  		console.log("Currency:" + mCurrency);


		/*
  		var stampName = $(this).children("#stamp_name").val();
  		var stampHours = $(this).children("#stamp_hours").val();
  		var projectId = $("#stamp_form_project_id").val();
  		var milestoneId = $("#stamp_form_milestone_id").val();
  		var stampDate = $(this).children("#datepicker").val();
  		*/
  		console.log("Saving milestone...");

  		var posting = $.post( "backend/backend.php", { req: "save_milestone", project_id: mProjectId, milestone_id: mId, status: mStatus, name: mName, estimateHours: mEstimateHours, priceHour: mPriceHour, priceProject: mPriceProject, currency:mCurrency } )
		 
	  	// Put the results in a div
	  	posting.done(function(data) {
	  		console.log(data);
	    	// $( "#timestamp_content" ).empty().append( data );
	    //	$( ".timestamps_bg .my_margin" ).empty().append( data );
	    	// attach save edit_milestone_data

	    //	attach_save_milestone_function();
	    	// 

	    	console.log("Post - Save Milestone done!");

	  	});

	  	console.log("Save milestone end!");

  		return false;
  	});
}

function populate_projects() {
	var posting = $.post( "backend/backend.php", { req: "get_projects" } )
 
  	// Put the results in a div
  	posting.done(function(data) {
    	$( "#projects_content" ).empty().append( data );
    	attach_milestone_functions();
    	attach_project_functions();
  	});
	 
	posting.fail(function( jqXHR, textStatus ) {
		// $( "#timestamp_content" ).empty().append( data );
	  alert( "Request failed: " + textStatus );
	});

}

function populate_finished_projects() {
	var posting = $.post( "backend/backend.php", { req: "get_finished_projects" } )
 
  	// Put the results in a div
  	posting.done(function(data) {
    	$( "#finished_projects_content" ).empty().append( data );
    	attach_milestone_functions();
    	attach_project_functions();
  	});
	 
	posting.fail(function( jqXHR, textStatus ) {
		// $( "#timestamp_content" ).empty().append( data );
	  alert( "Request failed: " + textStatus );
	});

}

function extractProjectAndMilestoneFromString() {
	var i = arguments[0];					// string
	var new_arr = new Array();
	var arr = i.split("_");
	var proj = arr[0].substr(5);			// remove "proj-" from string
	var mile_stone = arr[1].substr(10);		// remove "milestone-" from string
	
	new_arr["project"] = proj;
	new_arr["milestone"] = mile_stone;

	return new_arr;
}

$(document).ready(function(e) {
	populate_projects();
	populate_finished_projects();

	$.datepicker.setDefaults($.datepicker.regional['se']); 
	$.datepicker.setDefaults({ dateFormat: 'yy-mm-dd' });

	$("#datepicker").datepicker({dateFormat: "yy-mm-dd"});



	function showPopup(whichpopup){
        var docHeight = $(document).height(); //grab the height of the page
        var scrollTop = $(window).scrollTop(); //grab the px value from the top of the page to where you're scrolling
        projectId = 1;
        milestoneId = 1;

        var posting = $.post( "backend/backend.php", { req: "get_status", project_id: projectId, milestone_id: milestoneId } )
	 
	  	// Put the results in a div
	  	posting.done(function(data) {
	    	$('.popup'+whichpopup).empty().append( data );
	    	$('.popup'+whichpopup+' .btn').off().on("click", function(e) {
	    		var btn_status = $(this).data("set_status");
        		console.log(btn_status);
        		// Sätt status för milestone i DB här från vilket val man gjort.

        	});

	  	});
		 
		posting.fail(function( jqXHR, textStatus ) {
			// $( "#timestamp_content" ).empty().append( data );
		  alert( "Request failed: " + textStatus );
		});


        $('.overlay-bg').show().css({'height' : docHeight}); //display your popup background and set height to the page height
        $('.popup'+whichpopup).show().css({'top': scrollTop+20+'px'}); //show the appropriate popup and set the content 20px from the window top

    }
 
    // function to close our popups
    function closePopup(){
        $('.overlay-bg, .overlay-content').hide(); //hide the overlay
    }
 
    // timer if we want to show a popup after a few seconds.
    //get rid of this if you don't want a popup to show up automatically
 
   /*
    setTimeout(function() {
        // Show popup3 after 2 seconds
        showPopup(1);
    }, 2000);
 */
 
    // show popup when you click on the link
    $('.show-popup').click(function(event){
        event.preventDefault(); // disable normal link function so that it doesn't refresh the page
        var selectedPopup = $(this).data('showpopup'); //get the corresponding popup to show
         
        showPopup(selectedPopup); //we'll pass in the popup number to our showPopup() function to show which popup we want
    });
   
    // hide popup when user clicks on close button or if user clicks anywhere outside the container
    $('.close-btn, .overlay-bg').click(function(){
        closePopup();
    });
     
    // hide the popup when user presses the esc key
    $(document).keyup(function(e) {
        if (e.keyCode == 27) { // if user presses esc key
            closePopup();
        }
    });


});


